# Frontend (Nuxt.js)

Le site est rendu par [Nuxt.js](https://nuxtjs.org) qui permet la génération statique du site.

## Démarrer le Frontend

Installation des dépendances requises :
```bash
$ npm ci
```
Lancement en local sur localhost:3000 par défaut , avec rafraîchissement automatique :
```bash
$ npm run dev
```

Génération statique (pour un environnement de production) :
```bash
$ npm run build
$ npm run start
```

### Pour faire tourner le front en allant directement taper sur une instance de Sispea Contributeur

Il faut créer un fichier `.env` tel que :

```env
NO_CONTRIBUTOR_PROXY=true
CONTRIBUTOR_BASE_URL=http://localhost:8080/sispea
CMS_STRAPI_ROOT_URL=http://localhost:1337
```

La propriété `NO_CONTRIBUTOR_PROXY` permet de dire qu'on passe en mode « Accès direct aux services sur une instance Contributeur ».
Par défaut, cette propriété vaut `false`, ce qui veut dire que les URL construites sont en mode « Accès via le `contributor-data-proxy` ».

Pour l'accès direct aux services contributeur, il faut renseigner la propriété `CONTRIBUTOR_BASE_URL` pour un accès à la racine de l'application.
Exemples :
 - `http://localhost:8080/sispea`
 - `http://sispea.brgm-rec.fr/sispea`
 - `https://www.services.eaufrance.fr/sispea`
 - `https://sispea-refonte-diffusion.demo.codelutin.com`

Pour un accès normal (via le proxy), il faut renseigner la propriété `CONTRIBUTOR_PROXY_BASE_URL`.
Exemples :
 - `http://localhost:4444`
 - `http://172.17.0.3:4444`
 - `https://sispea-contributor-data-proxy.demo.codelutin.com`

La propriété `CMS_STRAPI_ROOT_URL` permet de définir l'adresse du CMS Strapi depuis lequel on veut récupérer les données éditoriales.
Exemples :
 - `http://localhost:1337` si il tourne en local
 - `https://sispea-cms-latest.demo.codelutin.com/`
