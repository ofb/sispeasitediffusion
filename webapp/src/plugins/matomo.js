export default ({ $config }) => {
    if (!$config.matomoURL || !$config.matomoID) {
      return;
    }

    const script = document.createElement('script')

    script.innerHTML = `
      <!-- Your Matomo Tag Manager Code -->
      var _paq = window._paq || [];
      /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
      _paq.push(['trackPageView']);
      _paq.push(['enableLinkTracking']);
      (function() {
          var u="${$config.matomoURL}";
          _paq.push(['setTrackerUrl', u+'matomo.php']);
          _paq.push(['setSiteId', '${$config.matomoID}']);
          var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
          g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
      })();
    `
  
    document.head.appendChild(script)
  }
