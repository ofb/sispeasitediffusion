import Vue from "vue";
import { Line, Bar, Pie } from "vue-chartjs/legacy";
import {
  Chart as ChartJS,
  registerables
} from "chart.js";

import ChartDataLabels from 'chartjs-plugin-datalabels';

ChartJS.register(
  ...registerables,
  ChartDataLabels
);

Vue.component("LineChart", {
  extends: Line,
});

Vue.component("BarChart", {
	extends: Bar,
});

Vue.component("PieChart", {
  extends: Pie,
});
