import Vue from 'vue'

Vue.filter('formatNumber', (value) => {
  return value.toLocaleString('fr-FR');
})