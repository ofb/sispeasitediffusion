export default function (req, res, next) {
  
    const redirects = [
        { "from": "/donnees/dernieres-donnees-disponibles$", "to": "/carte-interactive" },
        { "from": "/dernieres-donnees-disponibles$", "to": "/carte-interactive" },
        { "from": "/donnees/telechargement$", "to": "/pro/telechargement" },
        { "from": "/donnees/(.*)", "to": "/$1/" },
        { "from": "/gestion/services/(.*)", "to": "/gestion-services-$1" },
        { "from": "/gestion/services$", "to": "/gestion-services" },
        { "from": "/gestion/rpqs$", "to": "/gestion-rpqs" },
        { "from": "/indicateurs/indicateurs$", "to": "/indicateurs" },
        { "from": "/indicateurs/eau-potable$", "to": "/indicateurs" },
        { "from": "/indicateurs/assainissement-collectif$", "to": "/indicateurs" },
        { "from": "/indicateurs/assainissement-non-collectif$", "to": "/indicateurs" },
        { "from": "/indicateurs/mise-a-jour$", "to": "/indicateurs" },
        { "from": "/indicateurs/variables/(.*)", "to": "/indicateurs" },
        { "from": "/indicateurs/vos-questions$", "to": "/indicateurs" },
        { "from": "/mentions-l%C3%A9gales$", "to": "/mentions-legales" },
        { "from": "/panorama/rapports$", "to": "/mediatheque" },
        { "from": "/panorama/cartes(.*)", "to": "/mediatheque" },
        { "from": "/content/dbo5$", "to": "/glossaire" },
        { "from": "\/{2,}", "to": "/" },
        { "from": "/donnees$", "to": "/pro/telechargement" },
        { "from": "/gestion$", "to": "/mediatheque" },
        { "from": "/les-fuites$", "to": "/le-petit-cycle-de-l-eau" },
        { "from": "/node/2004$", "to": "/gestion-rpqs" },
        { "from": "/gestion/services/eau-nature-et-domestique$", "to": "/le-petit-cycle-de-l-eau" },
        { "from": "/gestion/services/eau-potable-assainissement$", "to": "/gestion-services-eau-potable-le-traitement" },
        { "from": "/gestion-services-eau-nature-et-domestique/petit-cycle$", "to": "/le-petit-cycle-de-l-eau" },
        { "from": "/gestion-services-eau-potable-assainissement/eaux-usees$", "to": "/le-petit-cycle-de-l-eau" },
        { "from": "/gestion-services-facture$", "to": "/facture" },
        { "from": "/gestion-services-services-quotidien$", "to": "/le-petit-cycle-de-l-eau" },
        { "from": "/gestion/rpqs/vos-questions$", "to": "/gestion-rpqs" },
        { "from": "/gestion/rpqs/references-reglementaires$", "to": "/gestion-services-textes-reglementaires" },
        { "from": "/gestion/documentation/guidestech$", "to": "/mediatheque" },
        { "from": "/gestion/documentation/observatoire$", "to": "/mediatheque" },
        { "from": "/gestion/documentation/liens-utiles$", "to": "/menu-liens-OFB" },
    ];
  
    const redirect = redirects.find(r => new RegExp(r.from).test(req.url));  
    if (redirect) {

        let redirection = req.url.replace(new RegExp(redirect.from), redirect.to);

        console.log(`301 redirection: ${req.url} => ${redirection}`);

        res.writeHead(301, { Location: redirection });
        res.end();
    } else {
        next();
    }
    
}