export function generateMeta(titre, description, seo) {
    let metaDescription = seo && seo.metaDescription || description,
        metaTitle = seo && seo.metaTitle || titre;

    if (metaDescription.length > 300) {
        metaDescription = limitText(metaDescription, 300);
    }

    return {
        title: `${ titre ? titre + ' | ': ''}Observatoire Sispea`,
        meta: [
            {
              hid: 'description',
              name: 'description',
              content: metaDescription
            },
            {
              hid: 'og:title',
              name: 'og:title',
              content: metaTitle
            },
            {
              hid: 'twitter:title',
              name: 'twitter:title',
              content: metaTitle
            },
            {
              hid: 'og:description',
              name: 'og:description',
              content: metaDescription
            },
            {
              hid: 'twitter:description',
              name: 'twitter:description',
              content: metaDescription
            },
            {
              hid: 'og:image',
              name: 'og:image',
              content: '~assets/logos/logo_eaufrance.png'
            },
            {
              hid: 'twitter:image',
              name: 'twitter:image',
              content: '~assets/logos/logo_eaufrance.png'
            }
        ],
        // link: [
        //     {
        //         hid: 'canonical',
        //         rel: 'canonical',
        //         href: canonicalUrl,
        //     },
        // ]
      };
}

function limitText (text, length) {
    let regexp = new RegExp(".{" + length + "}", "gi");
    return text.match(regexp)[0];
}