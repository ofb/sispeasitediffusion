const axios = require('axios');

export async function getRoutes(strapi) {
    
    // ********************
    // Fetch data from CMS
    // ********************

    const CMSprefix = process.env.CMS_STRAPI_ROOT_URL;

    // Fetch actualites
    const actusURL = CMSprefix + '/api/actualites'
    const strapiNews = await axios.get(actusURL).catch((e) => {
        logError(`[sitemap] Erreur lors de la récupération des actualités :`, e);
        return null;
    });
    let newsPath = strapiNews && strapiNews.data ? strapiNews.data.data.map(info => { return '/actualite/' + info.id }) : [];

    // Fetch articles
    const articlesURL = CMSprefix + '/api/articles'
    const strapiArticles = await axios.get(articlesURL).catch((e) => {
        logError(`[sitemap] Erreur lors de la récupération des articles :`, e);
        return null;
    });
    let articlesPath = strapiArticles && strapiArticles.data ? strapiArticles.data.data.map(info => { return '/article/' + info.id }) : [];

    // **********************
    // Fetch data from Sispea
    // **********************
        
    // Fetch indicators
    const indicatorsUrl = process.env.CONTRIBUTOR_PROXY_BASE_URL + '/indicator-definitions.json';
    const allIndicators = await axios.get(indicatorsUrl).catch((e) => {
        logError(`[sitemap] Erreur lors de la récupération des indicateurs :`, e);
        return null;
    });
    let indicatorsPath = allIndicators && allIndicators.data ? Object.entries(allIndicators.data.definitions).map(info => { return '/indicateurs/' + info[1].code }) : [];

    // Fetch agencies
    let agenciesPath = await _fetchTerritories('/all-agency-codes.json', 'agence');
    
    // Fetch collectivities
    let collectivitiesPath = await _fetchTerritories('/all-collectivity-ids.json', 'collectivite');

    // Fetch services
    let servicesPath = await _fetchTerritories('/all-service-ids.json', 'service');

    // Fetch municipalities
    let municipalitiesPath = await _fetchTerritories('/all-municipalities-insees.json', 'commune');

    // Fetch regions
    let regionsPath = await _fetchTerritories('/all-region-codes.json', 'region');

    // Fetch departements
    let departementsPath = await _fetchTerritories('/all-department-codes.json', 'departement');
  
    let files = [
        ...newsPath, 
        ...articlesPath,
        ...indicatorsPath, 
        ...agenciesPath,
        ...collectivitiesPath,
        ...servicesPath,
        ...municipalitiesPath,
        ...regionsPath,
        ...departementsPath
    ]

    return files;

}

async function _fetchTerritories(jsonUrl, path) {
    let territoryUrl = process.env.CONTRIBUTOR_PROXY_BASE_URL + jsonUrl;
    let allTerritories = await axios.get(territoryUrl).catch((e) => {
        logError(`[sitemap] Erreur lors de la récupération des ${path} :`, e);
        return null;
    });
    let territoriesPath = allTerritories ? allTerritories.data.map(info => { return '/'+path+'/' + info }) : [];
    return territoriesPath;
}

// Logs

function logError (errorMessage, errorTrace) {
    if (process.server) {
        // Exécutions côté serveur
        process.winstonLog.error(errorMessage, errorTrace)
    } else {
        // Exécutions côté client
        // FIXME LK : essayer d'ajouter un middelware pour logger les erreurs côtés client : https://catalins.tech/create-custom-api-endpoints-in-nuxt
        console.error(errorMessage, errorTrace)
    }
}