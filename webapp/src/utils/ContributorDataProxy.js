import axios from 'axios';
import Parser from 'rss-parser';

/* Récupération de contenu de Sispea Contributeur */
function computeUrl($config, directUri, proxyUri) {
    const result = $config.noContributorProxy
        ? `${$config.contributorDirectBaseUrl}${directUri}`
        : `${$config.contributorProxyBaseUrl}${proxyUri}`;
    return result;
}

/* Récupération de contenu externes à Sispea Contributeur */
function computeExternalUrl($config, directUrl, proxyUri) {
    const result = $config.noContributorProxy
        ? `${directUrl}`
        : `${$config.contributorProxyBaseUrl}${proxyUri}`;
    return result;
}

function logError (errorMessage, errorTrace) {
    if (process.server) {
        // Exécutions côté serveur
        process.winstonLog.error(errorMessage, errorTrace)
    } else {
        // Exécutions côté client
        console.error(errorMessage, errorTrace)
    }
}

export async function getIndicatorDefinitions($config) {
    const url = computeUrl($config, `/opendata/get-indicator-definitions-json.action`, `/indicator-definitions.json`);
    const { data } = await axios.get(url);
    return data.definitions;
}

export async function getVariableDefinitions($config) {
    const url = computeUrl($config, `/opendata/get-variable-definitions-json.action`, `/variable-definitions.json`);
    const { data } = await axios.get(url);
    return data.definitions;
}

export async function getMunicipality($config, municipalityInsee) {
    const url = computeUrl($config, `/opendata/get-municipality-json.action?insee=${municipalityInsee}`, `/municipalities/${municipalityInsee}.json`);
    const { data } = await axios.get(url);
    return data;
}

export async function getService($config, serviceId) {
    const url = computeUrl($config, `/opendata/get-service-json.action?id=${serviceId}`, `/services/${serviceId}.json`);
    const { data } = await axios.get(url);
    return data;
}

export async function getCollectivity($config, collectivityId) {
    const url = computeUrl($config, `/opendata/get-collectivity-json.action?id=${collectivityId}`, `/collectivities/${collectivityId}.json`);
    const { data } = await axios.get(url);
    return data;
}

export async function getDepartment($config, departmentCode) {
    const url = computeUrl($config, `/opendata/get-department-json.action?code=${departmentCode}`, `/departments/${departmentCode}.json`);
    const { data } = await axios.get(url);
    return data;
}

export async function getRegion($config, regionCode) {
    const url = computeUrl($config, `/opendata/get-region-json.action?code=${regionCode}`, `/regions/${regionCode}.json`);
    const { data } = await axios.get(url);
    return data;
}

export async function getAgency($config, agencyCode) {
    const url = computeUrl($config, `/opendata/get-agency-json.action?code=${agencyCode}`, `/agencies/${agencyCode}.json`);
    const { data } = await axios.get(url);
    return data;
}

export async function getFrance($config) {
    const url = computeUrl($config, `/opendata/get-france-json.action`, `/france.json`);
    const { data } = await axios.get(url);
    return data;
}

export async function getDepartmentGeojson($config, departmentCode) {
    const url = computeUrl($config, `/opendata/get-department-geojson.action?code=${departmentCode}`, `/departments/${departmentCode}.geojson`);
    const { data } = await axios.get(url);
    return data;
}

export async function getRegionGeojson($config, regionCode) {
    const url = computeUrl($config, `/opendata/get-region-geojson.action?code=${regionCode}`, `/regions/${regionCode}.geojson`);
    const { data } = await axios.get(url);
    return data;
}

export async function getCollectivityGeojson($config, collectivityId) {
    const url = computeUrl($config, `/opendata/get-collectivity-geojson.action?code=${collectivityId}`, `/collectivities/${collectivityId}.geojson`);
    const { data } = await axios.get(url);
    return data;
}

export async function getServiceGeojson($config, serviceId) {
    const url = computeUrl($config, `/opendata/get-service-geojson.action?code=${serviceId}`, `/services/${serviceId}.geojson`);
    const { data } = await axios.get(url);
    return data;
}

export async function getFranceGeojson($config) {
    const url = computeUrl($config, `/opendata/get-france-geojson.action`, `/france.geojson`);
    const { data } = await axios.get(url);
    return data;
}

export async function getMunicipalityGeojson($config, municipalityInsee) {
    const url = computeUrl($config, `/opendata/get-municipality-geojson.action?insee=${municipalityInsee}`, `/municipalities/${municipalityInsee}.geojson`);
    const { data } = await axios.get(url);
    return data;
}

export async function getEauPotableProjection($config) {
    const url = computeUrl($config, `/opendata/get-competence-projections-json.action?competence=EauPotable`, `/projections/EauPotable.json`);
    const { data } = await axios.get(url);
    return data;
}

export async function getAssainissementCollectifProjection($config) {
    const url = computeUrl($config, `/opendata/get-competence-projections-json.action?competence=AssainissementCollectif`, `/projections/AssainissementCollectif.json`);
    const { data } = await axios.get(url);
    return data;
}

export async function getAssainissementNonCollectifProjection($config) {
    const url = computeUrl($config, `/opendata/get-competence-projections-json.action?competence=AssainissementNonCollectif`, `/projections/AssainissementNonCollectif.json`);
    const { data } = await axios.get(url);
    return data;
}

export async function getYearlyExtractionFiles($config) {
    const url = computeUrl($config, `/opendata/get-yearly-extraction-files-json.action`, `/documents/yearly-extraction-files.json`);
    const data = await axios.get(url).catch((e) => {
        logError(`Error while querying the CDP (/${url}) : `, e)
    });
    return data && data.data;
}

export async function getServiceMembersCompositionFiles($config) {
    const url = computeUrl($config, `/opendata/get-service-members-composition-files-json.action`, `/documents/service-members-composition-files.json`);
    const data = await axios.get(url).catch((e) => {
        logError(`Error while querying the CDP (/${url}) : `, e)
    });
    return data && data.data;
}

export async function getCollectivityDescriptionFiles($config) {
    const url = computeUrl($config, `/opendata/get-collectivity-description-files-json.action`, `/documents/collectivity-description-files.json`);
    const data = await axios.get(url).catch((e) => {
        logError(`Error while querying the CDP (/${url}) : `, e)
    });;
    return data && data.data;
}

export async function getTarifFiles($config) {
    const url = computeUrl($config, `/opendata/get-tarifs-description-files-json.action`, `/documents/tarifs-description-files.json`);
    const data = await axios.get(url).catch((e) => {
        logError(`Error while querying the CDP (/${url}) : `, e)
    });
    return data && data.data;
}

export async function getBilanFiles($config) {
    const url = computeUrl($config, `/opendata/get-bilan-files-json.action`, `/documents/bilan-files.json`);
    const { data } = await axios.get(url);
    return data;
}

export async function getOieauNewsFeed($config) {
    const url = computeUrl($config, `https://www.oieau.fr/eaudanslaville/rss/actualites`, `/rss/oieau.rss`);
    const { data } = await axios.get(url);

    let parser = new Parser(),
        news = await parser.parseString(data);

    return news && news.items;
}

