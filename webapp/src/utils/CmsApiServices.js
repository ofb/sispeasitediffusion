import axios from 'axios';
const qs = require('qs');

// Pages non existantes mais appelées (par des robots) à retourner en 404 directement, sans appeler le CMS pour savoir si cela correspond à une page
const blacklistedPages = [
    '.env',
    'db.xml',
    'dns-query',
    'geoserver',
    'info.php',
    'rss',
    'server-status',
    'xmlrpc.php',
    'wp-login.php'
];


// Centralisation des requêtes vers l'api du CMS Strapi

// Gestion des appels depuis le côté client

async function queryFromClient($config, query, dataType) {
    const res = await axios.get(`${$config.cmsBaseExternalUrl}/api/${query}`).catch((e) => {
        logError(`Error ${e.statusCode} while fetching data from ${dataType} (/${query})`, e.errorMessage);
        return null;
    });
    return res && res.data;
}

// Logs

function logError(errorMessage, errorTrace) {
    if (process.server) {
        // Exécutions côté serveur
        process.winstonLog.error(errorMessage, errorTrace)
    } else {
        // Exécutions côté client
        // FIXME LK : essayer d'ajouter un middelware pour logger les erreurs côtés client : https://catalins.tech/create-custom-api-endpoints-in-nuxt
        console.error(errorMessage, errorTrace)
    }
}

// Accueil

export async function getHomePageInformation($strapi, isDraft) {
    const previewMode = isDraft ? '/preview' : '',
        query = `accueil-encart${previewMode}`,
        data = await $strapi.find(query);
    return data && data.data.attributes;
}

// Actualités

export async function getNews($strapi, pageNumber, pageSize, $config) {
    const query = `actualites?populate=*&sort[0]=publishedAt%3Adesc&pagination[page]=${pageNumber}&pagination[pageSize]=${pageSize}`;

    if (!process.server) {
        return queryFromClient($config, query, 'key news');
    }

    const data = await $strapi.find(query)
        .catch((e) => {
            logError(`Error ${e.statusCode} while retrieving data from key news (/${query})`, e.errorMessage)
        });
    return data;
}

export async function getOneNews($strapi, id, isDraft) {
    const previewMode = isDraft ? 'preview/' : '',
        query = `actualite/${previewMode}${id}?populate=*`,
        data = await $strapi.find(query)
            .catch((e) => {
                logError(`Error ${e.statusCode} while retrieving data from news (/${query})`, e.errorMessage)
            });

    return data;
}

// Articles ou Page
export async function getArticleOrPage($strapi, pageSlug, isDraft) {
    const previewMode = isDraft ? 'preview/' : '',
        pageApiQuery = `page/${previewMode}${pageSlug}`,
        articleApiQuery = `article/${previewMode}${pageSlug}`;

    const getArticle = function () {
        return $strapi.find(articleApiQuery)
            .catch((e) => {
                logError(`Error ${e.statusCode} while retrieving data from key article or page (/${articleApiQuery})`, e.errorMessage)
            });
    }

    const pageResult = await $strapi.find(pageApiQuery)
        .catch((e) => {
            if (e.statusCode == 404) {
                return getArticle();
            } else {
                logError(`Error ${e.statusCode} while retrieving data from key page (/${pageApiQuery})`, e.errorMessage)
            }
        });

    let page = pageResult && pageResult.data.attributes;

    return { page }
}

// Chiffres clés

export async function getKeyNumbers($strapi) {
    const query = 'chiffres-cles?sort=updatedAt:desc',
        data = await $strapi.find(query)
            .catch((e) => {
                logError(`Error ${e.statusCode} while retrieving data from key numbers (/${query})`, e.errorMessage)
            });
    return data && data.data;
}

export async function getOneKeyNumber($strapi, id, isDraft) {
    const previewMode = isDraft ? 'preview/' : '',
        query = `chiffres-cles/${previewMode}${id}?populate=illustration`,
        data = await $strapi.find(query)
            .catch((e) => {
                logError(`Error ${e.statusCode} while retrieving data from key number (/${query}) `, e.errorMessage)
            });
    return data && data.data.attributes;
}

// Espace professionnel

export async function getProAreaContent($strapi, $config, isDraft) {
    const previewMode = isDraft ? '/preview' : '',
        query = `professionnel-encart${previewMode}`;

    if (!process.server) {
        let data = await queryFromClient($config, query, "encart de l'espace professionnel");
        return data && data.data.attributes;
    }
    const data = await $strapi.find(query)
        .catch((e) => {
            logError(`Error ${e.statusCode} while retrieving data from pro area content (/${query})`, e.errorMessage)
        });

    return data && data.data.attributes;
}

export async function getProAreaLinks($strapi, $config) {
    const query = 'professionnel-liens';

    if (!process.server) {
        let data = await queryFromClient($config, query, "liens de l'espace professionnel");
        return data && data.data.attributes.liens;
    }

    const data = await $strapi.find(query)
        .catch((e) => {
            logError(`Error ${e.statusCode} while retrieving data from pro area links (/${query})`, e.errorMessage)
        });
    return data && data.data.attributes.liens;
}


// Glossaire

export async function getGlossaire($strapi) {
    const query = 'glossaire',
        data = await $strapi.find(query)
            .catch((e) => {
                logError(`Error ${e.statusCode} while retrieving data from glossary (/${query})`, e.errorMessage)
            });
    return data;
}

// Medias

export function getUploadedMediaURL($config, mediaUrl) {
    return `${$config.cmsBaseExternalUrl}${mediaUrl}`;
}

export function fixUploadedMediaURL($config, str) {
    // Les URLs des fichiers de la médiathèque sont à modifier pour être valides
    // Exemple : /uploads/a.img => http://url-cms/uploads/a.img
    return str.replace(/((?:src|srcset|href)=(?:["']))(\/uploads\/)/g, "$1" + $config.cmsBaseExternalUrl + "/..$2")
        .replace(/,(\/uploads\/)/g, "," + $config.cmsBaseExternalUrl + "$1");
}

// Menu

export async function getMenu($strapi, $config) {
    const query = 'menu-principal';

    if (!process.server) {
        let data = await queryFromClient($config, query, 'menu');
        return data && data.data.attributes.Sections;
    }

    const data = await $strapi.find(query)
        .catch((e) => {
            logError(`Error ${e.statusCode} while retrieving data from menu (/${query})`, e.errorMessage)
        });
    return data && data.data.attributes.Sections;
}

export async function getOFBLinks($strapi, $config) {
    const query = 'liens-vers-sites-ofb?populate=*';

    if (!process.server) {
        let data = await queryFromClient($config, query, 'liens OFB');
        return data;
    }

    const data = await $strapi.find(query)
        .catch((e) => {
            logError(`Error ${e.statusCode} while retrieving data from OFB links (/${query})`, e.errorMessage)
        });
    return data;
}

export async function getHomePageLinks($strapi, $config) {
    const query = 'accueil-liens';

    if (!process.server) {
        let data = await queryFromClient($config, query, "liens Page d'accueil");
        return data && data.data.attributes.liens;
    }

    const data = await $strapi.find(query)
        .catch((e) => {
            logError(`Error ${e.statusCode} while retrieving data from homepage links (/${query})`, e.errorMessage)
        });
    return data && data.data.attributes.liens;
}

export async function getFooterLinks($strapi, $config) {
    const query = 'menu-en-pied-de-page';

    if (!process.server) {
        let data = await queryFromClient($config, query, 'footer menu');
        return data && data.data.attributes.liens;
    }

    const data = await $strapi.find(query)
        .catch((e) => {
            logError(`Error ${e.statusCode} while retrieving data from footer menu (/${query})`, e.errorMessage)
        });
    return data && data.data.attributes.liens;
}


// Médiathèque

export async function getPublicationComponents($strapi, $config) {
    const query = `content-type-builder/components/rapport.publication`;

    if (!process.server) {
        return queryFromClient($config, query, 'publications components');
    }

    const data = await $strapi.find(query)
        .catch((e) => {
            logError(`Error ${e.statusCode} while retrieving data from publications components (/${query})`, e.errorMessage)
        });
    return data;
}

export async function getPublicationTypes($strapi, $config) {
    const query = `content-type-builder/content-types/api::publication.publication`;


    if (!process.server) {
        return queryFromClient($config, query, 'publication types');
    }

    const data = await $strapi.find(query)
        .catch((e) => {
            logError(`Error ${e.statusCode} while retrieving data from publication types (/${query})`, e.errorMessage)
        });
    return data;
}

export async function getAllDocuments($strapi, pageNumber, pageSize, $config) {
    return findDocuments($config, $strapi, "", "",null, "", pageNumber, pageSize);
}

export async function getLastRapport($strapi, $config) {
    return findDocuments($config, $strapi, "", ['Rapport Sispea'],null, "", 1, 1);
}

export async function getRapportIntroduction($strapi, $config, isDraft) {
    const previewMode = isDraft ? '/preview' : '',
        query = `rapports-introduction${previewMode}`;

    if (!process.server) {
        let data = await queryFromClient($config, query, 'rapports-introduction');
        return data && data.data.attributes.liens;
    }

    const data = await $strapi.find(query)
        .catch((e) => {
            logError(`Error ${e.statusCode} while retrieving data from rapports-introduction (/${query})`, e.errorMessage)
        });
    return data && data.data.attributes.introduction;
}

export async function getAllRapportsSapin($strapi, pageNumber, pageSize, $config) {
    return findDocuments($config, $strapi, "", ['Rapport Loi SAPIN'], null,"", pageNumber, pageSize);
}

export async function getLoiSapinIntroduction($strapi, $config, isDraft) {
    const previewMode = isDraft ? '/preview' : '',
        query = `loi-sapin-introduction${previewMode}`;

    if (!process.server) {
        let data = await queryFromClient($config, query, 'loi-sapin-introduction');
        return data && data.data.attributes.liens;
    }

    const data = await $strapi.find(query)
        .catch((e) => {
            logError(`Error ${e.statusCode} while retrieving data from loi-sapin-introduction (/${query})`, e.errorMessage)
        });
    return data && data.data.attributes.introduction;
}

export async function searchDocuments($config, term, themes, types, years, pageNumber, pageSize) {

    return await findDocuments($config, undefined, term, themes, types, years, pageNumber, pageSize)
        .then(allDocuments => {
            return allDocuments;
            // let result = {};
            // result.meta = allDocuments.meta;

            // let data = [];
            // allDocuments.data.forEach(document => {
            //     let publications = document.attributes.publication.filter(publi => types.includes(publi.type));
            //     document.attributes.publication = publications;
            //     if(publications.length > 0) {
            //         data.push(document);
            //     }
            // });
            // result.data = data;    
            // return result;
        });
}

async function findDocuments($config, $strapi, term, themes, publicationTypes, years, pageNumber, pageSize) {

    let themeFilter = {};
    if (themes != "") {

        themeFilter = {
            $or: []
        }

        themes.forEach((theme) => {

            themeFilter.$or.push({
                type: {
                    $eq: theme
                }
            })
        });
    }

    let termFilter = {};
    if (term != "") {
        termFilter = {
            $or: [
                {
                    intitule: {
                        $containsi: term
                    }
                },
                {
                    description: {
                        $containsi: term
                    }
                },
                {
                    publication: {
                        intitule: {
                            $containsi: term
                        }
                    }
                },
                {
                    publication: {
                        description: {
                            $containsi: term
                        }
                    }
                }
            ]
        };
    }

    let yearsFilter = {};
    if (years != "") {
        let firstDay = years[0] + "-01-01";
        let lastDay = years[1] + "-12-31";
        yearsFilter = {
            $and: [
                {
                    date_publication: {
                        $gte: firstDay
                    }
                },
                {
                    date_publication: {
                        $lte: lastDay
                    }
                }
            ]
        }
    }

    let filters = {

        $and: [
            termFilter,
            themeFilter,
            yearsFilter
        ]
    };

    if (publicationTypes) {
        filters.publication = {
            type: { $in: publicationTypes }
        }
    }

    let qsQuery = qs.stringify({
        populate: {
            publication: {
                populate: ['fichier']
            }
        },
        sort: ['date_publication:desc', 'createdAt:desc'],
        pagination: {
            page: pageNumber,
            pageSize: pageSize,
        },
        filters: filters
    });

    let query = `publications?${qsQuery}`

    if (!process.server) {
        return queryFromClient($config, query, 'publications')
            .then(documents => {
                return documents;
            });
    }

    if ($strapi) {
        return await $strapi.find(query)
            .then(documents => {
                return documents;
            })
            .catch((e) => {
                logError(`Error ${e.statusCode} while retrieving data from publications (/${qsQuery})`, e.errorMessage)
            });
    }
}

export async function findAllPublicationYears($strapi) {

    const query = 'publications/years';

    const data = await $strapi.find(query)
        .catch((e) => {
            logError(`Error ${e.statusCode} while retrieving data from publications (/${query})`, e.errorMessage)
        });
    return data;
}

export async function findArticles($config, term, pageNumber, pageSize) {

    let query = `?filters[$or][0][titre][$containsi]=${term}` +
        `&filters[$or][1][contenu][$containsi]=${term}` +
        `&pagination[pageNumber]=${pageNumber}` +
        `&pagination[pageSize]=${pageSize}`;

    const articles = queryFromClient($config, `articles${query}`, 'key news');

    return articles;
}

export async function findNews($config, term, pageNumber, pageSize) {

    let query = `?filters[$or][0][titre][$containsi]=${term}` +
        `&filters[$or][1][contenu][$containsi]=${term}` +
        `&pagination[pageNumber]=${pageNumber}` +
        `&pagination[pageSize]=${pageSize}`;

    const news = queryFromClient($config, `actualites${query}`, 'key news');

    return news;
}


export async function findDocumentsByTerm($config, term, pageNumber, pageSize) {
    return findDocuments($config, undefined, term, "", null, "", pageNumber, pageSize);
}

export async function getPublication($strapi, id) {
    let query = `publications/${id}?populate[publication][populate][0]=fichier`

    const data = await $strapi.find(query)
        .catch((e) => {
            logError(`Error ${e.statusCode} while retrieving data from pro area links (/${query})`, e.errorMessage)
        });

    return data && data.data;
}

// FAQ

export async function getAllFAQQuestions($strapi, $config) {
    let qsQuery = qs.stringify({
        populate: {
            themes: {
                populate: {
                    sousthemes: {
                        populate: {
                            precisions: {
                                populate: '*'
                            },
                            questions: '*',
                            icone: '*'
                        }
                    },
                    questions: '*',
                    icone: '*'
                }
            },
            questions: '*',
            icone: '*'
        },
        sort: ['id'],
    });

    let query = `faq-questions?${qsQuery}`;

    if (!process.server) {
        return queryFromClient($config, query, 'questions')
            .then(questions => {
                return questions;
            });
    }

    if ($strapi) {
        return await $strapi.find(query)
            .then(questions => {
                return questions;
            })
            .catch((e) => {
                logError(`Error ${e.statusCode} while retrieving data from questions (/${qsQuery})`, e.errorMessage)
            });
    }
}

export async function searchFAQ($config, term) {
    let query = `faq-question/search?term=${term}`

    return queryFromClient($config, query, 'questions')
        .then(questions => {
            return questions;
        });
}

// Qualité de l'eau

export async function getWaterQualityInformation($strapi, $config, isDraft) {
    const previewMode = isDraft ? '/preview' : '',
        query = `qualite-de-l-eau-encart${previewMode}`;
    let data;

    if (!process.server) {
        data = await queryFromClient($config, query, 'waterQualityInformation')
            .then(waterQualityInformation => {
                return waterQualityInformation;
            });
    } else {
        data = await $strapi.find(query)
            .catch((e) => {
                logError(`Error ${e.statusCode} while retrieving data from Water quality information (/${query})`, e.errorMessage)
            });;
    }
    return data && data.data.attributes;
}


export function checkBlacklistedPages(page) {
    return blacklistedPages.includes(page);
}