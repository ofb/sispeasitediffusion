import axios from 'axios';

const transport = axios.create({
    withCredentials: true
})

export function authenticate(sispeaUrl, login, password) {
    const authenticateUrl = sispeaUrl + '/authentication/web-authenticate.action';

    return transport.post(authenticateUrl, null, {params: {
      'login': login,
      'password': password
    }});
}

export function getDetailedTarif(sispeaUrl, token,  id_exercise, id_organism) {

    var jsonQuery = '{"serviceId":' + id_organism +'}';
    const url = sispeaUrl + '/tarifs/get-detailed-tarifs-json.action';

    return transport.post(url, null, {
    headers: {
       'Content-Type': 'text/plain',
       'Connection': 'keep-alive',
       'X-Sispea-Token': token,
       'Sec-Fetch-Site': 'same-origin'
    },params: {
        'exerciseId': id_exercise,
        'filterJson' : jsonQuery,
        'paginationJsonData' : {"pageNumber":0,"pageSize":-1}
    }});

}

export function searchTerritories(sispeaUrl, term, competences, pageNumber) {
    const url = sispeaUrl + '/opendata/search-territories-json.action';
    return _search(url, term, competences, pageNumber);
}

export function searchMunicipalities(sispeaUrl, term, competences, pageNumber) {
    const url = sispeaUrl + '/opendata/search-municipalities-json.action';
    return _search(url, term, competences, pageNumber);
}

export function searchDepartments(sispeaUrl, term, competences, pageNumber) {
    const url = sispeaUrl + '/opendata/search-departments-json.action';
    return _search(url, term, competences, pageNumber);
}

export function searchRegions(sispeaUrl, term, competences, pageNumber) {
    const url = sispeaUrl + '/opendata/search-regions-json.action';
    return _search(url, term, competences, pageNumber);
}

export function searchServices(sispeaUrl, term, competences, pageNumber) {
    const url = sispeaUrl + '/opendata/search-services-json.action';
    return _search(url, term, competences, pageNumber);
}

export function searchCollectivities(sispeaUrl, term, competences, pageNumber) {
    const url = sispeaUrl + '/opendata/search-collectivities-json.action';
    return _search(url, term, competences, pageNumber);
}

export function searchAgencies(sispeaUrl, term, competences, pageNumber) {
    const url = sispeaUrl + '/opendata/search-agencies-json.action';
    return _search(url, term, competences, pageNumber);
}

function _search(url, term, competences, pageNumber) {
    return transport.post(url, null, {
        headers: {
           'Content-Type': 'text/plain',
           'Connection': 'keep-alive',
           'Sec-Fetch-Site': 'same-origin'
        },params: {
            'filter': {
                'term': term,
                'competences': competences
            },
            'nbResults': 6,
            'pageNumber': pageNumber
        }});
}

export function sendNewQuestion(sispeaUrl, token, indicatorCode, email, question) {

    const url = sispeaUrl + '/indicators/send-question-about-indicator-json.action';

    return transport.post(url, null, {
        headers: {
        'Content-Type': 'text/plain',
        'Connection': 'keep-alive',
        'X-Sispea-Token': token,
        'Sec-Fetch-Site': 'same-origin'
        },params: {
            'indicatorCode': indicatorCode,
            'mailSender': email,
            'question': question
        }});

}

export function retrieveMunicipalityGeojson(codeINSEE) {

    const url = '/contributor-data-proxy/municipalities/' + codeINSEE + '.geojson';

    return transport.get(url, null, {
        headers: {
        'Content-Type': 'text/plain',
        'Connection': 'keep-alive',
        'Sec-Fetch-Site': 'same-origin'
        }});
}

export function retrieveDepartmentGeojson(code) {

    const url = '/contributor-data-proxy/departments/' + code + '.geojson';

    return transport.get(url, null, {
        headers: {
        'Content-Type': 'text/plain',
        'Connection': 'keep-alive',
        'Sec-Fetch-Site': 'same-origin'
        }});

}

export function retrieveRegionGeojson(code) {

    const url = '/contributor-data-proxy/regions/' + code + '.geojson';

    return transport.get(url, null, {
        headers: {
        'Content-Type': 'text/plain',
        'Connection': 'keep-alive',
        'Sec-Fetch-Site': 'same-origin'
        }});

}

export function retrieveFranceGeojson() {

    const url = '/contributor-data-proxy/france.geojson';

    return transport.get(url, null, {
        headers: {
        'Content-Type': 'text/plain',
        'Connection': 'keep-alive',
        'Sec-Fetch-Site': 'same-origin'
        }});

}


export function getMunicipalityInfo(codeInsee) {

    const url = '/contributor-data-proxy/municipalities/' + codeInsee + '.json';

    return transport.get(url, null, {
        headers: {
        'Content-Type': 'text/plain',
        'Connection': 'keep-alive',
        'Sec-Fetch-Site': 'same-origin'
        }});
}

export function getDepartmentInfo(departmentCode) {

    const url = '/contributor-data-proxy/departments/' + departmentCode + '.json';

    return transport.get(url, null, {
        headers: {
        'Content-Type': 'text/plain',
        'Connection': 'keep-alive',
        'Sec-Fetch-Site': 'same-origin'
        }});
}

export function getRegionInfo(regionCode) {

    const url = '/contributor-data-proxy/regions/' + regionCode + '.json';

    return transport.get(url, null, {
        headers: {
        'Content-Type': 'text/plain',
        'Connection': 'keep-alive',
        'Sec-Fetch-Site': 'same-origin'
        }});
}

export function getFranceInfo() {
    const url = '/contributor-data-proxy/france.json';

    return transport.get(url, null, {
        headers: {
        'Content-Type': 'text/plain',
        'Connection': 'keep-alive',
        'Sec-Fetch-Site': 'same-origin'
        }});
}

