import path from 'path';
import { getRoutes } from "./src/utils/seo";
import { format, transports } from 'winston';

const DailyRotateFile = require('winston-daily-rotate-file');
const timezoned = () => {
  return new Date().toLocaleTimeString('fr-FR', {
    timeZone: 'Europe/Paris'
  });
};

export default {

  globalName :'sispea',

  srcDir: 'src/',

  // This option lets you define the development or production mode of Nuxt.js
  dev: process.env.NODE_ENV !== 'production',

  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: true,

  render: {
    // Désactive l'injection des scripts (permet de ne plus avoir la partie cliente en JS)
    // injectScripts: false
  },

  // Target: https://go.nuxtjs.dev/config-target
  target: 'server',

  server: {
    host: '0.0.0.0'
  },

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: "Observatoire des services publics de l'eau et de l'assainissement",
    htmlAttrs: {
      lang: 'fr'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Observatoire des services publics de l\'eau et de l\'assainissement : prix de l\'eau et performance des services' },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [
      { rel: 'alternate icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'icon', type: 'image/png', href: '/favicon.png' },
      { rel: 'stylesheet', type: 'text/css', href: '/print.css', media: 'print'}
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['@/assets/css/main.scss', '@/assets/generated-font/sispea-icons.css'],
  /*
   ** Global style resources - https://www.npmjs.com/package/@nuxtjs/style-resources
   */
  styleResources: {
    scss: ['~/assets/css/variables.scss']
  },

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~plugins/filters.js',
    {src: '~plugins/chart.js', mode: 'client'},
    { src: '~plugins/matomo.js', mode: 'client' }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    '@aceforth/nuxt-optimized-images',
    '@nuxtjs/composition-api/module',
    // 'nuxt-fontagon', // Décommenter pour actualiser la police d'icônes
    'nuxt-compress',
    [
      '@nuxtjs/i18n',
      {
        vueI18nLoader: true,
        defaultLocale: 'fr',
        locales: [
          {
             code: 'fr',
             iso: 'fr-FR',
             file: 'fr.json'
          }
        ],
        detectBrowserLanguage: false, // Permet d'éviter qu'un cookie "i18n_redirected=fr" soit positionné et que ça désactive la mise en cache des pages produites
        langDir: 'locales'
      }
     ]
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
      '@nuxtjs/markdownit',
      '@nuxtjs/style-resources',
      '@nuxtjs/strapi',
      '@nuxt/content',
      'nuxt-lazy-load',
      '@nuxtjs/proxy',
      ['nuxt-compress', {
        brotli: {
          threshold: 8192,
        },
      }],
      'nuxt-winston-log',
      '@nuxtjs/sitemap',
      '@nuxtjs/robots',
  ],

  router: {
  },

  strapi: {
    url: process.env.CMS_STRAPI_API_URL
          || ( process.env.CMS_STRAPI_ROOT_URL ?
              process.env.CMS_STRAPI_ROOT_URL + "/api" :
              'https://sispea-cms-latest.demo.codelutin.com' ),
    prefix: '/api',
    version: 'v4',
    expires: '1d'
  },

  markdownit: {
      runtime: true, // Support `$md()`
      preset: 'default',
      linkify: true,
      typographer: true,
      breaks: true,
      injected: true,
  },

  optimizedImages: {
    optimizeImages: true
  },



  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    analyze: false,
    extractCSS: true,

    html:{
      minify:{
        collapseBooleanAttributes: true,
        decodeEntities: true,
        minifyCSS: true,
        minifyJS: true,
        processConditionalComments: true,
        removeEmptyAttributes: true,
        removeRedundantAttributes: true,
        trimCustomFragments: true,
        useShortDoctype: true,
        minifyURLs: true,
        removeComments: true,
        removeEmptyElements: true,
        preserveLineBreaks: false,
        collapseWhitespace: true
      }
    },

    // Group all JS into a single file.
    optimization: {
      splitChunks: {
        cacheGroups: {
           // Merge all the CSS into one file
          styles: {
            name: 'diffusion',
            test: /\.(s?css|vue)$/,
            chunks: 'all',
            enforce: true,
            reuseExistingChunk: true
          }
        }

      }
    },
     // Group all JS into a single file.
    //  optimization: {
    //   splitChunks: {
    //     chunks: 'async',
    //   }
    // },
    splitChunks: {
      pages: true,
      vendor: false,
      commons: false,
      runtime: false,
      layouts: false
    },

  },

  // parameters config
  publicRuntimeConfig: {
    // accessible from both client and server
    sispeaURL: process.env.CONTRIBUTOR_BASE_URL || 'http://localhost:8080/sispea',
    cmsBaseExternalUrl: process.env.CMS_STRAPI_ROOT_URL || 'http://localhost:1337',
    contributorProxyBaseUrl: process.env.CONTRIBUTOR_PROXY_BASE_URL || "http://localhost:4444",
    ignKey: process.env.IGN_KEY || 'decouverte',
    matomoURL: process.env.MATOMO_URL,
    matomoID: process.env.MATOMO_SITE_ID
  },

  privateRuntimeConfig: {
    // accessible from server only
    apiSecret: process.env.API_SECRET,
    brgmProxyHost: process.env.BRGM_PROXY_HOST,
    brgmProxyPort: process.env.BRGM_PROXY_PORT,
    contributorProxyBaseUrl: process.env.CONTRIBUTOR_PROXY_BASE_URL || "http://localhost:4444",
    contributorDirectBaseUrl: process.env.CONTRIBUTOR_BASE_URL || "http://localhost:8080/sispea",
    noContributorProxy: process.env.NO_CONTRIBUTOR_PROXY || false,
    appVersion : process.env.APP_VERSION
  },

  vue: {
    config: {
      ignoredElements: [/^content-/]
    }
  },

  messages: {
      loading: 'Chargement...',
      error_404: `Cette page n\\'existe pas`,
      server_error: 'Erreur de chargement du serveur',
  },

  iconFont: {
    fontName: "sispea-icons",
    classOptions: {
      "baseClass": "sispea-icons",
      "classPrefix": "icon"
    },
    style: "css",
    files: ["./src/assets/icons/*.svg"],
    dist: "./src/assets/generated-font"
  },

  proxy: {
    // contributor-data-proxy
    '/contributor-data-proxy': {
      // Supprime le /contributor-data-proxy avant de faire suivre l'appel
      pathRewrite: {
        '^/contributor-data-proxy' : ''
      },
      target: process.env.CONTRIBUTOR_PROXY_BASE_URL || "http://localhost:4444"
    }
    // // cms (strapi)
    // ,'/cms': {
    //   // Supprime le /cms avant de faire suivre l'appel
    //   pathRewrite: {
    //     '^/cms' : ''
    //   },
    //   target: process.env.CMS_BASE_URL || "http://cms:1337"
    // }
  },

  winstonLog: {
    loggerOptions: {
      format: format.combine(
        format.timestamp({ format: timezoned }),
        format.printf(
          info =>
            `${info.timestamp} [${info.level.toUpperCase()}]: ${info.message} [${process.platform},${process.pid}]`
        )
      ),
      transports: [
        new transports.Console(),
        new DailyRotateFile({
          filename: path.resolve(process.cwd(), './logs', `${process.env.NODE_ENV}.log`),
          datePattern: 'yyyy-MM-DD',
          prepend: true,
          localTime: true,
          level: process.env.NODE_ENV === 'development' ? 'debug' : 'error',
        })
      ]
    }
  },

  // SEO
  sitemap: {
    hostname: 'https://www.services.eaufrance.fr',
    path: "/sitemap.xml",
    routes: async() => {
      return getRoutes();
    },
    exclude: [
      '/status',
      '/sitemap',
      '/menu',
      '/pro',
      '/pro/publications',
      '/pro/publications/carte',
      '/pro/publications/guides',
      '/pro/publications/rapports',
      '/menu-liens-OFB'
    ]
  },

  robots: {
    UserAgent: '*',
    Allow: '/',
    Disallow: '',
    Sitemap: (req) => `https://${req.headers.host}/sitemap.xml`
  },

  serverMiddleware: [
    '~/utils/seoredirects.js'
  ]
}
