module.exports = {
  root: true,
  env: {
    node: true,
    browser: true
  },
  extends: [
    "eslint:recommended",
    "plugin:vue/recommended",
    "plugin:nuxt/recommended"
  ],
  globals: {
    $nuxt: true
  }
}