/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'Sispea - Site de diffusion',
  tagline: 'Sispea',
  url: 'https://gitlab.nuiton.org/',
  favicon: 'img/favicon.ico',
  organizationName: 'ofb', // Usually your GitHub org/user name.
  projectName: 'sispeasitediffusion', // Usually your repo name.

  baseUrl: '/',
  i18n: {
    defaultLocale: 'fr',
    locales: ['fr'],
  },
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  themeConfig: {
    navbar: {
      title: 'Sispea - Site de diffusion',
      logo: {
        alt: 'My Site Logo',
        src: 'img/favicon.ico',
      },
      items: [
        {
          type: 'doc',
          docId: 'Specifications_Fonctionnelles/Intro',
          position: 'left',
          label: 'Spécifications fonctionnelles',
        },
        {
          type: 'doc',
          docId: 'Specifications_Techniques/Intro',
          position: 'left',
          label: 'Spécifications techniques',
        },
        {
          to: '/changelog',
          label: 'Changelog',
          position: 'left'
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Docummentation du projet',
          items: [
            {
              label: 'Guide pour y participer',
              to: '/guide-documentation',
            },
          ],
        },
        {
          title: 'Liens du projet',
          items: [
            {
              label: 'Sources sur Gitlab',
              href: 'https://gitlab.nuiton.org/ofb/sispeasitediffusion/',
            },
            {
              label: 'Forge Redmine',
              href: 'https://forge.eaufrance.fr/projects/sispea-gsp',
            },
          ],
        },
      ],
    },
    colorMode: {
      disableSwitch: false,
      respectPrefersColorScheme: true,
      switchConfig: {
        darkIcon: '\u2600',
        darkIconStyle: {
          marginLeft: '2px',
          color: '#fff',
        },
        lightIcon: '\u263E',
        lightIconStyle: {
          marginLeft: '1px',
          color: '#fff',
        },
      },
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./src/js/sidebars.js'),
          editUrl: 'https://gitlab.nuiton.org/ofb/sispeasitediffusion/-/tree/master/documentation/',
          remarkPlugins: [require('mdx-mermaid')],
        },
        blog: {
          path: 'changelog',
          routeBasePath: 'changelog',
          showReadingTime: false,
          blogSidebarTitle: 'Versions',
          editUrl: 'https://gitlab.nuiton.org/ofb/sispeasitediffusion/-/tree/master/documentation/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
  plugins: [
    require.resolve('docusaurus-lunr-search'),
    './plugins/symbolicLinks.js'
  ],
};
