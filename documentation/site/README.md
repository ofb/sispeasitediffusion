# Documentation du projet

La documentation du projet est générée via l'outil [Docusaurus](https://docusaurus.io/).

### Installation

Installation des dépendances requises :
```bash
$ yarn
```
Lancement en local sur localhost:3000 par défaut, avec rafraîchissement automatique :
```bash
$ yarn start
```

Génération statique (pour un environnement de production) :
```bash
$ yarn build
```
Le site est généré dans le dossier `build`.
