
```mermaid
flowchart LR
    Accueil --- Apropos[À propos]
    Accueil --- Actualités
    Accueil --- Cartographie
    Accueil --- Indicateurs
    Accueil --- Chiffres[Chiffres clés]
    Accueil --- Ressources[Ressources pédagogiques]
    Ressources --- Ressource1[cycle de l'eau]
    Ressources --- Ressource2[services publics d'eau et d'assainissement]
    Ressources --- Ressource3[rapport prix et qualité du service]
    Ressources --- Ressource4[facture d'eau]
    Accueil --- Pro[Espace pro ouvert au grand public]
    Pro --- Téléchargement
    Pro --- Publication
    Pro --- Réglementation
```

## URL

| URL                         | page côté diffusion         | comportement                               |
| --------------------------- | --------------------------- | ------------------------------------------ |
| /france                     | france/index.vue            | redirection vers la dernière année         |
| /france/[ANNEE]             | france/_year.vue            | rendu                                      |
| /agence/[CODE]              | agency/_code/index.vue      | redirection vers la dernière année         |
| /agence/[CODE]/[ANNEE]      | agency/_code/_year.vue      | rendu                                      |
| /region/[CODE]              | region/_code/index.vue      | redirection vers la dernière année         |
| /region/[CODE]/[ANNEE]      | region/_code/_year.vue      | rendu                                      |
| /departement/[CODE]         | departement/_code/index.vue | redirection vers la dernière année         |
| /departement/[CODE]/[ANNEE] | departement/_code/_year.vue | rendu                                      |
| /collectivite/[ID]          | collectivite/_id/index.vue  | redirection vers la dernière année         |
| /collectivite/[ID]/[ANNEE]  | collectivite/_id/_year.vue  | rendu                                      |
| /service/[ID]               | service/_id/index.vue       | redirection vers la dernière année         |
| /service/[ID]/[ANNEE]       | service/_id/_year.vue       | rendu                                      |
| /commune/[INSEE]            | commune/_insee/index.vue    | redirection vers la dernière année         |
| /commune/[INSEE]/[ANNEE]    | commune/_insee/_year.vue    | rendu                                      |
| /indicateurs                | indicateurs/index.vue       | rendu - liste des indicateurs et variables |
| /indicateurs/[CODE]         | indicateurs/_code.vue       | rendu                                      |
| /variables/[CODE]           | variables/_code.vue         | rendu                                      |


Redirections à prévoir en frontal
* /donnees/[...] -> /[...]
* /indicateurs/[code]/[ep|eu|anc] -> /variables/[code]

À valider potentiellement avec l'OFB :
* redirection vers la dernière année ou rendu ?
* suppression [ep|eu|anc] pour les variables
* redirection vers /variables pour les variables
