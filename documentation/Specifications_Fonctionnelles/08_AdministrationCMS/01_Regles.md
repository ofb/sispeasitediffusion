---
title: Règles générales
---

## Mise en page


### Images


Choix de l'alignement des images : 
* ![](/img/captures-strapi/align-text.png) **Alignement avec le texte** : l'image est alignée selon l'alignement du texte. Pour la placer à gauche, centrée ou à droite, il faut donc utiliser les boutons permettant d'aligner le texte
* ![](/img/captures-strapi/align-centre.png) **Alignement centré** : l'image est centrée
* ![](/img/captures-strapi/align-side.png) **Alignement sur le côté** : l'image est placée sur la droite du contenu, côte à côte du texte.

### Barre horizontale

Il suffit de taper trois tiret `-` pour que cela soit transformé en ligne horizontale : 

---

