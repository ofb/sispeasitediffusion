---
title: Aides contributeur
---

# Pages d'aide pour Sispea Contributeur

Ces pages d'aide sont destinées uniquement à Contributeur.
Elles ne sont pas visibles sur le site grand public.


:::caution

**Le mode brouillon est activé pour les pages d'aide.**  
Il faut donc bien veiller à **publier** une page pour qu'elle soit visible sur Sispea Contributeur (Le bouton est situé en haut à droite).  

Si elle est indiquée en version brouillon (dans la partie droite de l'écran), elle ne sera pas visible.

:::

## Champs du formulaire de modification d'une page

| Champ | Description |
| - | - |
|Titre||
|Identifiant|Généré automatiquement, mais modifiable si besoin. Attention, toute modification doit être répercuté dans Sispea Contributeur, dans le composant `showDocProductionHelp` utilisé pour l'afficher|
|Contenu| |


