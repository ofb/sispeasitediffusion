# Liens vers sites OFB

Les liens vers d'autres sites OFB sont afficher dans l'entête de page lorsqu'on clique sur le bouton `Sites`.

Ils sont automatiquement triés par catégorie, puis par nom lorsqu'ils sont affichés.

:::info

Le mode brouillon n'est pas activé pour les liens OFB.  
Tout lien renseigné est visible sur le site grand public.

:::

## Champs du formulaire de modification d'un chiffre clé

| Champ | Précision |
| - | - |
|Nom||
|Lien||
|Catégorie|Pour ajouter une nouvelle catégorie, veuillez demander à l'administrateur technique du site|
|Image||
