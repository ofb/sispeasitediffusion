# Pages

Une page est accessible directement en ajoutant son identifiant à l'URL racine du site.
Par exemple : https://sispea2.brgm-rec.fr/mentions-legales
L'identifiant est visible dans la colonne **Identifiant** de la liste des pages.

Une page peut également être référencée depuis un menu.

:::caution

**Le mode brouillon est activé pour les pages.**  
Il faut donc bien veiller à **publier** une page pour qu'elle soit visible du grand public (Le bouton est situé en haut à droite).  
Le bouton `Voir sur le site` permet d'aller la consulter.

Si elle est indiquée en version brouillon (dans la partie droite de l'écran), elle ne sera pas visible sur le site. Il est possible d'en voir un aperçu en cliquant sur le bouton `Prévisualiser le brouillon` à droite de l'écran.

:::

## Champs du formulaire de modification d'une page

| Champ | Précision |
| - | - |
|Titre||
|Identifiant|Généré automatiquement, mais modifiable si besoin. Attention, toute modification de l'identifiant modifie l'URL d'accès à l'article, et peut donc entraîner des problèmes de référencement SEO de l'article|
|Contenu| |
|Illustration|Illustration provenant de la médiathèque, qui apparaît dans l'entête de la page.|

