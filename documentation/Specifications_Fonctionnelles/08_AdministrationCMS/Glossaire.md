# Glossaire

Le glossaire est accessible depuis une [page dédiée](https://sispea2.brgm-rec.fr/glossaire).


:::caution

**Le mode brouillon est activé pour les entrées de glossaire.**  
Il faut donc bien veiller à **publier** une entrée pour qu'elle soit visible du grand public (Le bouton est situé en haut à droite).  
Le bouton `Voir sur le site` permet d'aller le consulter.

Si elle est indiquée en version brouillon (dans la partie droite de l'écran), elle ne sera pas visible sur le site. 

:::

## Champs du formulaire de modification d'un chiffre clé

| Champ | Précision |
| - | - |
|Libellé||
|Définition||
|Source||
