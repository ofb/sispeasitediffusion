---
title: Actualités
---

# Actualités

Les actualités sont visibles sur le site grand public sur la page d'accueil et dans la section [Actualités](https://sispea2.brgm-rec.fr/actualite).  
Elles sont triées par date de publication, en ordre décroissant.

Une actualité est accessible directement via une URL avec son identifiant.  
L'identifiant est visible dans la colonne **ID** de la liste des actualités.  
En ajoutant à l'url du site *'actualite/'* et l'identifiant de l'article.  
Par exemple : https://sispea2.brgm-rec.fr/actualite/1

:::caution

**Le mode brouillon est activé pour les actualités.**  
Il faut donc bien veiller à **publier** une actualité pour qu'elle soit visible du grand public (Le bouton est situé en haut à droite).  
Le bouton `Voir sur le site` permet d'aller la consulter.

Si elle est indiquée en version brouillon (dans la partie droite de l'écran), elle ne sera pas visible sur le site. Il est possible d'en voir un aperçu en cliquant sur le bouton `Prévisualiser le brouillon` à droite de l'écran.

:::

## Champs du formulaire de modification d'une actualité

| Champ | Précision |
| - | - |
|Titre||
|Contenu||
|Illustrations|Illustrations provenant de la médiathèque, qui apparaît dans l'entête de la page.|
|Image ancien site|Lorsque l'actualité a été importée depuis le site Drupal, l'image de l'actualité est stockée sur le serveur, en dehors de la médiathèque de Strapi|