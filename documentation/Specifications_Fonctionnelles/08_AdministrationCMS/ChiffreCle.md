---
title: Chiffre clé
---

# Chiffre clé

Les chiffres clés sont visibles sur le site grand public dans une [rubrique dédiée](https://sispea2.brgm-rec.fr/chiffres).

Un chiffre clé est accessible directement via une URL avec son identifiant.
L'identifiant est visible dans la colonne **ID** de la liste des chiffres clés.
En ajoutant à l'url du site *'chiffres/'* et l'identifiant du chiffre clé.
Par exemple : https://sispea2.brgm-rec.fr/chiffres/1

:::caution

**Le mode brouillon est activé pour les chiffres clés.**  
Il faut donc bien veiller à **publier** un chiffre clé pour qu'il soit visible du grand public (Le bouton est situé en haut à droite).  
Le bouton `Voir sur le site` permet d'aller le consulter.

Si il est indiqué en version brouillon (dans la partie droite de l'écran), il ne sera pas visible sur le site. Il est possible d'en voir un aperçu en cliquant sur le bouton `Prévisualiser le brouillon` à droite de l'écran.

:::

## Champs du formulaire de modification d'un chiffre clé

| Champ | Précision |
| - | - |
|Titre||
|Valeur||
|Unité||
|Année||
|Description||
|Illustration|Illustration provenant de la médiathèque, qui apparaît en bas de page.|
