---
title: Accès à l'administration
---

# Accès à l'interface d'administration du CMS

## Accès

L'interface est accessible à l'adresse https://sispea2.brgm-rec.fr/cms/acces-ofb/

Les identifiants d'accès à l'interface d'administration sont propres à chaque utilisateur.

Si l'interface n'est pas affichée par défaut en français, il est possible de le configurer en cliquant sur son nom en bas de gauche de l'écran, puis en cliquant sur **Profile**. La liste déroulante **Interface language / Langue de l'interface** est située en bas de l'écran.
Tous les libellés ne sont pas traduis, certains resteront en anglais.

## Gestion du contenu éditorial

Une fois connecté à l'interface d'administration, le contenu éditorial se trouve dans la section **Content manager**.

