# Article

Un article est accessible directement en ajoutant son identifiant à l'URL racine du site.  
Par exemple : https://sispea2.brgm-rec.fr/le-petit-cycle-de-leau  
L'identifiant est visible dans la colonne **Identifiant** de la liste des articles.

Un article peut également être référencé depuis un menu.

:::caution

**Le mode brouillon est activé pour les articles.**  
Il faut donc bien veiller à **publier** un article pour qu'il soit visible du grand public (Le bouton est situé en haut à droite).  
Le bouton `Voir sur le site` permet d'aller le consulter.

Si il est indiqué en version brouillon (dans la partie droite de l'écran), il ne sera pas visible sur le site. Il est possible d'en voir un aperçu en cliquant sur le bouton `Prévisualiser le brouillon` à droite de l'écran.

:::

## Champs du formulaire de modification d'un article

| Champ | Précision |
| - | - |
|Titre||
|Identifiant|Généré automatiquement, mais modifiable si besoin. Attention, toute modification de l'identifiant modifie l'URL d'accès à l'article, et peut donc entraîner des problèmes de référencement SEO de l'article|
|Contenu| |
|Illustration|Illustration provenant de la médiathèque, qui apparaît dans l'entête de la page.|
|Image provenant de l'ancien site Drupal|Lorsque l'actualité a été importée depuis le site Drupal, l'image de l'actualité est stockée sur le serveur, en dehors de la médiathèque de Strapi|