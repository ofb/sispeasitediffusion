Des webservices sont disponibles dans Sispea Contributeur pour mettre à disposition les données.

Sispea Diffusion utilise cette API pour récupérer les données de référentiels et les données annualisées des entités de gestion.

Les services côté contributeur permettant de récupérer ces données sont classés en 3 catégories :
 - les services à usage interne permettant de récupérer les listes d'identifiants
 - les services permettant de précalculer des données (leur résultat est stocké dans `contributor-data-proxy/prefetched-files`) ;
 - les services destinés à être utilisés directement par le front.


## Récupération des identifiants/codes

| Donnée                 | URL                                                 | Chemin `raw-data`                 |
| ---------------------- | --------------------------------------------------- | --------------------------------- |
| Codes des agences      | `/opendata/get-all-agency-codes-json.action`        | `/all-agency-codes.json`          |
| Codes des régions      | `/opendata/get-all-region-codes-json.action`        | `/all-region-codes.json`          |
| Codes des départements | `/opendata/get-all-department-codes-json.action`    | `/all-department-codes.json`      |
| IDs des collectivités  | `/opendata/get-all-collectivity-ids-json.action`    | `/all-collectivity-ids.json`      |
| IDs des services       | `/opendata/get-all-service-ids-json.action`         | `/all-service-ids.json`           |
| Codes des communes     | `/opendata/get-all-municipality-insees-json.action` | `/all-municipalities-insees.json` |
| Codes des indicateurs  | `/opendata/get-all-indicator-codes-json.action`     | `/all-indicator-codes.json`       |

## Récupération des données précalculées

| Donnée                                   | URL                                                            | Chemin `prefetched-files`      | Script                            | Fréquence de mise à jour |
| ---------------------------------------- | -------------------------------------------------------------- | ------------------------------ | --------------------------------- | ------------------------ |
| Consolidées FRANCE                       | `/opendata/get-france-json.action`                             | `/france.json`                 | `/fetch-france.sh`                | 1x/semaine               |
| Découpe FRANCE + régions (GeoJson)       | `/opendata/get-france-geojson.action`                          | `/france.geojson`              | `/fetch-france.sh`                | 1x/semaine               |
| Consolidées Agence                       | `/opendata/get-agency-json.action?code=CODE`                   | `/agencies/CODE.json`          | `/fetch-agencies.sh`              | 1x/semaine               |
| Consolidées Région                       | `/opendata/get-region-json.action?code=CODE`                   | `/regions/CODE.json`           | `/fetch-regions.sh`               | 1x/semaine               |
| Découpe Région + départements (GeoJson)  | `/opendata/get-region-geojson.action?code=CODE`                | `/regions/CODE.geojson`        | `/fetch-regions.sh`               | 1x/semaine               |
| Consolidées Département                  | `/opendata/get-department-json.action?code=CODE`               | `/departments/CODE.json`       | `/fetch-departments.sh`           | 1x/semaine               |
| Découpe Département + communes (GeoJson) | `/opendata/get-department-geojson.action?code=CODE`            | `/departments/CODE.geojson`    | `/fetch-departments.sh`           | 1x/semaine               |
| Collectivité                             | `/opendata/get-collectivity-json.action?id=ID`                 | `/collectivities/ID.json`      | `/fetch-collectivities.sh`        | 1x/jour si mise à jour   |
| Service                                  | `/opendata/get-service-json.action?id=ID`                      | `/services/ID.json`            | `/fetch-services.sh`              | 1x/jour si mise à jour   |
| Commune                                  | `/opendata/get-municipality-json.action?insee=CODE`            | `/municipalities/CODE.json`    | `/fetch-municipalities.sh`        | 1x/jour si mise à jour   |
| Découpe Commune + voisines (GeoJson)     | `/opendata/get-municipality-geojson.action?insee=CODE`         | `/municipalities/CODE.geojson` | `/fetch-municipalities.sh`        | 1x/jour si mise à jour   |
| Définitions des indicateurs              | `/opendata/get-indicator-definitions-json.action`              | `/indicator-definitions.json`  | `/fetch-indicator-definitions.sh` | 1x/jour                  |
| Définitions des variables                | `/opendata/get-variable-definitions-json.action`               | `/variable-definitions.json`   | `/fetch-variable-definitions.sh`  | 1x/jour                  |
| PDF des indicateurs                      | `/opendata/get-indicator-definition-pdf-card.action?code=CODE` | `/indicators/CODE.pdf`         | `/fetch-indicator-cards.sh`       | 1x/jour                  |

## Utilisation en direct depuis le front

| Fonctionnalité       | URL                                                                          |
| -------------------- | ---------------------------------------------------------------------------- |
| Recherche de dommune | `/opendata/search-municipalities-json.action?term=TERM&nbResults=NB_RESULTS` |




