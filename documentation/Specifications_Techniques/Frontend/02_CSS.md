# CSS

## Emplacements des définitions CSS

L'emplacement où ajouter des déclarations CSS dépend de leur portées.

|Portée|Où placer ce css ?|
| - | - |
|Style concernant uniquement un composant|Dans le fichier `.vue` du composant, dans une balise `<style scoped>`|
|Style commun à plusieurs composants|Dans le dossier `src/assets/css` <br/> Le fichier principal `main.scss` regroupe plusieurs sous-fichiers partiels (préfixés par "_") se limitant à un contexte ou une thématique particuliers. <br/>Il faut donc ajouter les définitions dans un des fichiers partiels ou en créant un si nécessaire, sans oublier de l'importer dans `main.scss`|
|Valeur globale (ex: couleur)|Dans le fichier `src/assets/css/variables.scss` |

## Framework

Le framework **Pico.css** est utilisé pour fournir des règles d'affichages basiques et des composants élémentaires.
Ce framework a été choisi car il se limite à l'essentiel, et cela permet de restreindre la taille des fichiers téléchargés par le navigateur internet pour pouvoir affiché le site.
Ainsi, le CSS utilisé ne contient pas, ou peu, de classes inutilisés.

La documentation de **Pico.css** est disponible à l'adresse : https://picocss.com/docs/

Voici une brève liste des classes et composants pouvant être utiles pour Sispea

### Affichage en colonnes

```html
<div class="grid">
  <div>1</div>
  <div>2</div>
  <div>3</div>
  <div>4</div>
</div>
```

### Boutons

```html
<button>Bouton display=block</button>
<a href="#" role="button">Bouton display=inline</a>
```

Des déclinaisons des boutons sont présentes avec les classes : `secondary` , `contrast`, `outline`

## Nommage des classes

Autant que possible, il est conseillé de suivre le nommage [BEM (Block Element Modifier)](http://getbem.com/introduction/).
Cela permet facilement d'identifier l'élément et la situation concernés par la classe.

Exemple : `class="header__logo--hover"`

