# Composants

## Bloc d'actualité

```html
<ApercuActualite :news="news"> />
```

|Nom    | valeurs |
|-------|-|
|`news` | Données de l'actualité, provenant de l'API du backend Strapi |

## Carrousel d'actualités

```html
<DiaporamaActualites :collection="news" />
```

|Nom    | valeurs |
|-------|-|
|`collection` | Données des actualités, provenant de l'API du backend Strapi |


## Icône

```html
<Icon name="search" size="big" />
```

**Attributs** :

|Nom    | valeurs |
|-------|-|
|`name` | Nom de l'icône. <br/> Correspond au nom (sans l'extension) du fichier source de l'icône, présent dans `webapp/src/assets/icons` |
|`size` | `small`, `regular`, `big`, `huge`|

NB : La police d'icônes utilisée dans l'application est automatiquement générée au build, à l'aide de la dépendance **nuxt-fontagon**. Pour ajouter une nouvelle icône, il suffit donc de la placer au format svg dans le dossier `webapp/src/assets/icons`.
