# Configuration

La configuration principale de `Nuxt.js` se fait dans le fichier `nuxt.config.js` (Cf https://nuxtjs.org/docs/directory-structure/nuxt-config#nuxtconfigjs).


# Server side rendering

Le contenu des pages est généré sur le serveur et restitué au front en réponse aux requêtes http (Cf https://nuxtjs.org/docs/features/deployment-targets#server-hosting)

Il faut éviter dans ce cas d'utiliser des `Nuxtlink` qui permettent la navigation entre pages sans interroger le serveur.

