
```mermaid
flowchart LR
    classDef subgraph_padding fill:none,stroke:none
    classDef dockerized fill:#ff9955

    Visiteur

    subgraph Serveur VMRN11
        Apache
        subgraph subgraph_padding [ ]
            subgraph Sispea Diffusion
                WebappProxyCache[Webapp Proxy Cache<br/>nginx]
                Webapp[Webapp<br/>nodejs / Vue.js]
                CMS[CMS<br/>Strapi]
                ContributorDataProxy[Contributor Data Proxy<br/>nginx]
            end
        end
    end

    subgraph Serveur VMRM17
        subgraph ContributeurBDDs
            direction TB
            bdd_Contributeur[(BDD 'sispea_producteur'<br/>PostgreSQL<br/>Base actuelle de la recette)]
            bdd_Contributeur2[(BDD 'sispea_v2'<br/>PostgreSQL - Postgis<br/>Future base de la recette)]
        end
        bdd_CMS[(BDD CMS<br/>PostgreSQL)]
    end

    subgraph Serveur VMRA03
        Contributeur[Sispea Contributeur]
    end


    subgraph Légende
        LegendDocker[Conteneurs Docker]
    end

    Visiteur<-->Apache
    Apache<-- :8081 -->WebappProxyCache
    Webapp<-->WebappProxyCache
    Webapp<-->ContributorDataProxy
    ContributorDataProxy<-->Contributeur
    Webapp<-->Contributeur
    Webapp<-->CMS
    bdd_Contributeur<-. OU .->Contributeur
    Contributeur<-- OU -->bdd_Contributeur2
    CMS<-->bdd_CMS

    class subgraph_padding subgraph_padding
    class ContributeurBDDs subgraph_padding
    class Webapp dockerized
    class WebappProxyCache dockerized
    class ContributorDataProxy dockerized
    class CMS dockerized
    class LegendDocker dockerized
```


```mermaid
flowchart LR
    subgraph Navigateur
        Visiteur
    end

    subgraph Serveur ?
        Drupal[Drupal Sispea Diffusion]
    end

    subgraph Serveur VMRA03
        DonneesContributeur[Sispea Contributeur]
    end

    subgraph Serveur VMRM17
        bdd_Contributeur[(BDD<br/>PostgreSQL<br/>Contributeur)]
        bdd_Copie_Contributeur[(BDD<br/>PostgreSQL<br/>Copie Contributeur)]
    end

    bdd_Drupal[(BDD<br/>Drupal)]

    Visiteur<-->Drupal
    Drupal<-->bdd_Copie_Contributeur
    Drupal<-->bdd_Drupal
    DonneesContributeur<-->bdd_Contributeur
    bdd_Contributeur-->bdd_Copie_Contributeur
```
