---
title: Configuration
---

# Configuration du CMS Strapi pour Sispea Diffusion

## Sécurité

Pour que des données du CMS puissent être exposées ou modifiées via une API, il faut ajouter les autorisations nécessaires.

Pour cela, il faut se rendre dans l'administration de strapi sur l'écran **Paramètres** > **Rôles & Permissions**.

Pour chaque rôle (dont le visiteur non authentifié **Public**), les droits peuvent être définis pour chaque donnée pouvant être mise à disposition via l'API.

## Éditeur de contenu texte enrichi

L'éditeur par défaut de Strapi n'est pas tout à fait adapté aux besoins de Sispea Diffusion :
* L'édition n'est pas Wysiwyg : le contenu est rédigé en markdown brut et un bouton permet de prévisualisation le rendu. Ce n'est pas aussi intuitif qu'un éditeur Wysiwyg
* Le contenu renvoyé par l'API est au format markdown, ce qui implique un traitement supplémentaire côté front pour transformer le markdown en html équivalent.

Il a dont été remplacé par l'éditeur **[CKEditor5](https://ckeditor.com/)**.  
Le contenu est rendu directement en html par l'API.  
Il est possible d'importer un des [packages officiels](https://ckeditor.com/docs/ckeditor5/latest/builds/guides/integration/installation.html#npm), et de suivre la [documentationd e Strapi](https://strapi.io/documentation/developer-docs/latest/guides/registering-a-field-in-admin.html#setup) pour interfacer cet éditeur avec les autres fonctionnalités du CMS.  
Mais cet installation via les packages officiels n'est pas satisfaisante car les fonctionnalités présentes par défaut ne correspondent pas tout à fait aux besoins de mise en forme pour Sispea. Et pour modifier ces fonctionnalités, cela impose de créer sa [propre configuration](https://ckeditor.com/docs/ckeditor5/latest/builds/guides/development/custom-builds.html) et de la déployer sur un dépôt.  
Le plugin **[Strapi CKEditor5 plugin](https://github.com/Roslovets-Inc/strapi-plugin-ckeditor5)** a donc été choisi pour importer l'éditeur et pouvoir le configurer plus facilement .
