---
title: Introduction
hide_title: true
---

import StrapiReadme from '../../../../../cms/app/README.md'

<StrapiReadme />

## Pré-requis du serveur pour déployer Strapi en production

[Cf la documentation de Strapi](https://strapi.io/documentation/developer-docs/latest/setup-deployment-guides/deployment.html#general-guidelines)