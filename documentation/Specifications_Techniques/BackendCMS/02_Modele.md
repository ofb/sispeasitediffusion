---
title: Modèle de données
---

## Création d'un nouveau type de données

Depuis l'interface d'administration de Strapi, en allant dans **Content Types builder** et en séléctionnant **Créer un type de collection**.

À la création d'un type de données, sont créés par Strapi :

* une table en base de donnée, avec a minima les attributs par défauts suivants :

```mermaid
classDiagram
    class Mytype{
        id : serial4
        published_at : timestamptz
        created_by : int4
        created_at : timestamptz
        updated_at : timestamptz
    }
```

* des fichiers de configuration dans le dossier `api` de Strapi, avec :
  * `/config/routes.json` : 
  * `/controllers/mytype.js` : 
  * `/models/mytype.settings.json` : 
  * `/models/mytype.js` : Importe `/models/mytype.settings.json`
  * `/services/mytype.js` : 

Exemple de fichier `/models/mytype.settings.json`
```json
{
  "kind": "collectionType",
  "collectionName": "mytype",
  "info": {
    "name": "Actualités",
    "description": ""
  },
  "options": {
    "increments": true,
    "timestamps": true,
    "draftAndPublish": true
  },
  "pluginOptions": {},
  "attributes": {
    "Title": {
      "type": "string"
    },
    "Content": {
      "type": "richtext"
    }
  }
}
```

## Modifications d'un type de données

Procédure pour appliquer des changements sur le modèle de données :
* En local, réaliser les modifications souhaitées depuis l'interface de Strapi, ou modifier directement le fichier `/models/mytype.settings.json`
* Envoyer les changements sur le repository du projet et déployer une nouvelle version de l'application.
* En recette ou en production :
  * Arrêter Strapi
  * Mettre à jour l'application
  * Redémarrer Strapi. Les modifications sont alors détectées et appliquées sur la table correspondant en BDD


:::caution Suppression d'un attribut

En cas de suppression d'un attribut du modèle, la colonne correspondante est conservée dans la table de la BDD du type de donnée.  
Les éventuelles valeurs déjà renseignées pour cet attribut (qui a été supprimé) sont conservées également.

Si par la suite, un attribut est ajouté avec le même nom, une migration est réalisée par Strapi si le type du nouvel attribut est compatible avec celui de l'ancien.  
Si ce n'est pas le cas, cela provoquera une erreur lors de la tentative de migration des données existantes :

```
error error: alter table "mytype" alter column "mycolumn" type integer using ("mycolumn"::integer) - invalid input syntax for type integer: "Lorem ipsum"
```

:::

:::caution Suppression d'un type de données

Si un type de données est supprimé de Strapi, la table correspondante en base de données n'est pas supprimée non plus.

:::
