## Architecture en recette

TODO : reprendre schéma de l'architecture avec les noms de serveurs

## Configurations serveur

Dossier de l'appli : `/applications/projets/sispea/`
URL : https://sispea2.brgm-rec.fr

## Mettre à jour l'application

Lancer le script `/applications/projets/sispea/update-and-restart.sh` qui : 
* récupére les images disponibles sur registry.nuiton.org
* Arrête et relance le docker-compose qui contient les différents modules de l'application


Pour vérifier la date de dernière mise à jour de l'application : `cat /applications/projets/sispea/.last-update-and-restart`
