---
title: Introduction
---

# Spécifications techniques

Le site est rendu au visiteur en frontend par des pages html générées par [**Nuxt.js**](https://nuxtjs.org/).

Les données alimentant le site de diffusion proviennent de plusieurs backend :
* [**Sispea Contributeur**](https://services.eaufrance.fr/sispea/)
* [**CMS Strapi**](https://strapi.io/)

| Donnée | SISPEA Contributeur | CMS Strapi |
| - | :-: | :-: |
| Actualités |  |   X |
| Articles |  | X |
| Indicateurs | X | |
| Page |  | X |
| Variables | X | |

