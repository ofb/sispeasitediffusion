#!/bin/bash

mkdir -p ${ENV_OUTPUT_FOLDER}/logs

TIMESTAMP=`date +"%Y-%m-%d"`
LOG_FILE=${ENV_OUTPUT_FOLDER}/logs/weekly-${TIMESTAMP}.log
ERROR_FILE=${ENV_OUTPUT_FOLDER}/logs/weekly-${TIMESTAMP}.err

echo "`date +"%Y/%m/%d %H:%M:%S"` : Mise à jour hebdomadaire depuis l'URL ${ENV_CONTRIBUTOR_URL}"
echo "`date +"%Y/%m/%d %H:%M:%S"` : Mise à jour hebdomadaire depuis l'URL ${ENV_CONTRIBUTOR_URL}" >> ${LOG_FILE}

if [ ${BRGM_PROXY_HOST} ] && [ ${BRGM_PROXY_PORT} ]
then
    BRGM_PROXY=${BRGM_PROXY_HOST}:${BRGM_PROXY_PORT}
    echo "`date +"%Y/%m/%d %H:%M:%S"` : Utilisation du proxy '${BRGM_PROXY}' activée pour la récupération des données" >> ${LOG_FILE}
fi

cd /root/scripts

./fetch-indicator-definitions.sh  ${ENV_CONTRIBUTOR_URL} ${ENV_OUTPUT_FOLDER} ${ENV_RAW_DATA_FOLDER} ${BRGM_PROXY} >> ${LOG_FILE} 2>> ${ERROR_FILE}
./fetch-variable-definitions.sh   ${ENV_CONTRIBUTOR_URL} ${ENV_OUTPUT_FOLDER} ${ENV_RAW_DATA_FOLDER} ${BRGM_PROXY} >> ${LOG_FILE} 2>> ${ERROR_FILE}
./fetch-municipalities.sh         ${ENV_CONTRIBUTOR_URL} ${ENV_OUTPUT_FOLDER} ${ENV_RAW_DATA_FOLDER} ${BRGM_PROXY} >> ${LOG_FILE} 2>> ${ERROR_FILE}
./fetch-services.sh               ${ENV_CONTRIBUTOR_URL} ${ENV_OUTPUT_FOLDER} ${ENV_RAW_DATA_FOLDER} ${BRGM_PROXY} >> ${LOG_FILE} 2>> ${ERROR_FILE}
./fetch-collectivities.sh         ${ENV_CONTRIBUTOR_URL} ${ENV_OUTPUT_FOLDER} ${ENV_RAW_DATA_FOLDER} ${BRGM_PROXY} >> ${LOG_FILE} 2>> ${ERROR_FILE}
./fetch-departments.sh            ${ENV_CONTRIBUTOR_URL} ${ENV_OUTPUT_FOLDER} ${ENV_RAW_DATA_FOLDER} ${BRGM_PROXY} >> ${LOG_FILE} 2>> ${ERROR_FILE}
./fetch-regions.sh                ${ENV_CONTRIBUTOR_URL} ${ENV_OUTPUT_FOLDER} ${ENV_RAW_DATA_FOLDER} ${BRGM_PROXY} >> ${LOG_FILE} 2>> ${ERROR_FILE}
./fetch-agencies.sh               ${ENV_CONTRIBUTOR_URL} ${ENV_OUTPUT_FOLDER} ${ENV_RAW_DATA_FOLDER} ${BRGM_PROXY} >> ${LOG_FILE} 2>> ${ERROR_FILE}
./fetch-france.sh                 ${ENV_CONTRIBUTOR_URL} ${ENV_OUTPUT_FOLDER} ${ENV_RAW_DATA_FOLDER} ${BRGM_PROXY} >> ${LOG_FILE} 2>> ${ERROR_FILE}
./fetch-competence-projections.sh ${ENV_CONTRIBUTOR_URL} ${ENV_OUTPUT_FOLDER} ${ENV_RAW_DATA_FOLDER} ${BRGM_PROXY} >> ${LOG_FILE} 2>> ${ERROR_FILE}
./fetch-documents.sh              ${ENV_CONTRIBUTOR_URL} ${ENV_OUTPUT_FOLDER} ${ENV_RAW_DATA_FOLDER} ${BRGM_PROXY} >> ${LOG_FILE} 2>> ${ERROR_FILE}

echo "`date +"%Y/%m/%d %H:%M:%S"` : Mise à jour hebdomadaire terminée"
echo "`date +"%Y/%m/%d %H:%M:%S"` : Mise à jour hebdomadaire terminée" >> ${LOG_FILE}

