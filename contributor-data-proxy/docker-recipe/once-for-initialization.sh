#!/bin/bash

mkdir -p ${ENV_OUTPUT_FOLDER}/logs

TIMESTAMP=`date +"%Y-%m-%d"`
LOG_FILE=${ENV_OUTPUT_FOLDER}/logs/init-${TIMESTAMP}.log
ERROR_FILE=${ENV_OUTPUT_FOLDER}/logs/init-${TIMESTAMP}.err

echo "`date +"%Y/%m/%d %H:%M:%S"` : Mise à jour initiale depuis l'URL ${ENV_CONTRIBUTOR_URL}"
echo "`date +"%Y/%m/%d %H:%M:%S"` : Mise à jour initiale depuis l'URL ${ENV_CONTRIBUTOR_URL}" >> ${LOG_FILE}

if [[ -n ${BRGM_PROXY} ]]
then
    BRGM_PROXY=${BRGM_PROXY_HOST}:${BRGM_PROXY_PORT}
    echo "`date +"%Y/%m/%d %H:%M:%S"` : Utilisation du proxy '${BRGM_PROXY}' activée pour la récupération des données" >> ${LOG_FILE}
fi

cd /root/scripts

./initialize-municipalities.sh ${ENV_CONTRIBUTOR_URL} ${ENV_OUTPUT_FOLDER} ${ENV_RAW_DATA_FOLDER} ${BRGM_PROXY} >> ${LOG_FILE} 2>> ${ERROR_FILE}

echo "`date +"%Y/%m/%d %H:%M:%S"` : Mise à jour initiale terminée"
echo "`date +"%Y/%m/%d %H:%M:%S"` : Mise à jour initiale terminée" >> ${LOG_FILE}

