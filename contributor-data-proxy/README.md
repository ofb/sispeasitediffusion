# Contributor Data Proxy

Le rôle de cette brique est de récupérer et stocker les informations qui peuvent être précalculés à partir de Sispea Contributeur.

Ce module est composé des parties suivantes :
 - `scripts` : les scripts permettant la récupération des contenus ;
 - `docker-recipe` : le nécessaire pour construire l'image Docker ;
 - `docker-compose.yml` : le fichier pour le déploiement en Docker Compose ;
 - `.env` : le fichier contenant les paramètres ajustables (non versionné sur Git) ;
 - `samples` : des scripts et fichiers qui peuvent encore servir.

 ## Les scripts

 Chaque script prend 3 paramètres (toujours les mêmes) tel que :

 ```bash
 ./script.sh ${SERVER} ${OUTPUT} ${RAW_FOLDER}
 ```

| Nom        | Description                            | Valeur par défaut              | 
| ---------- | -------------------------------------- | ------------------------------ |
| SERVER     | URL pour accéder à Sispea Contributeur | `http://localhost:8080/sispea` |
| OUTPUT     | Chemin pour écrire les fichiers        | `prefetched-files`             |
| RAW_FOLDER | Dossier pour les fichiers temporaires  | `/tmp/raw-files`               |

Il est possible de n'appeler le script qu'avec le 1 premier ou les 2 premiers paramètres.
Exemple :

```bash
./script.sh https://sispea-refonte-diffusion.demo.codelutin.com
```
ou encore :

```bash
./script.sh http://localhost:8080/sispea /tmp/toto
```

## L'image Docker

L'image se base sur `nginx` pour exposer les fichiers précalculés.
À cette image on rajoute l'éxécution automatique des scripts (via `cron`) pour faire les mises à jour.

Le dossier `docker-recipe` contient les fichiers suivants :
 - `Dockerfile` : la recette Docker ;
 - `40-start-crond.sh` : le script éxécuté au démarrage qui permet de lancer `crond` ;
 - `nginx.conf` : le fichier de configuration `nginx` ;
 - `daily.sh` : le script éxécuté quotidiennement (mise à jour partielle) ;
 - `weekly.sh` : le script éxécuté hebdomadairement (mise à jour complète).

On peut construire l'image manuellement tel que :

```bash
docker build \
  --build-arg DAILY_CRON_EXPR="5       2       *       *       *" \
  --build-arg WEEKLY_CRON_EXPR="5       3       *       *       6" \
  -t contributor-data-proxy:latest .
```

Il faut fournir à la contruction les expression cron pour l'éxécution quotidienne et hebdomadaire.

Pour démarrer ensuite il faut monter le volume des scripts et préciser les variables d'environnement :

```bash
docker run --rm --name contributor-data-proxy \
  --volume ${PWD}/../scripts:/root/scripts:ro \
  --volume ${PWD}/../prefetched-files:/usr/share/nginx/html \
  --env ENV_CONTRIBUTOR_URL=http://192.168.222.111:8080/sispea \
  --env ENV_OUTPUT_FOLDER=/usr/share/nginx/html \
  --env ENV_RAW_DATA_FOLDER=/tmp/raw-data \
  -p 80:80 contributor-data-proxy:latest
```

## Docker Compose & `.env`

Docker Compose permet de prendre en charge la construction et le déploiement de l'image.

Le fichier `docker-compose.yml` contient la configuration par défaut, qui peut être surchargée via le fichier `.env`.

Exemple de fichier `.env` :

```properties
CONTRIBUTOR_URL=https://sispea-refonte-diffusion.demo.codelutin.com

# cron :     min	hour	day	month	weekday	
DAILY_CRON="5       2       *       *       *"
WEEKLY_CRON="5       3       *       *       6"
```

Seule la propriété `CONTRIBUTOR_URL` est obligatoire. Les autres ont des valeurs par défaut.

Une fois le fichier `.env` créé il suffit de démarrer la stack via :
```bash
docker-compose up -d --build
```

L'image tourne alors sur le port 4444 : http://localhost:4444


Pour déclencher une mise à jour manuelle il suffit d'éxécuter la commande suivante :
```bash
docker exec -it contributor-data-proxy "/root/daily.sh"
```
ou
```bash
docker exec -it contributor-data-proxy "/root/weekly.sh"
```
