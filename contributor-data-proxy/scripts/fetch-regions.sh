#!/bin/bash
SERVER=${1:-http://localhost:8080/sispea}
OUTPUT=${2:-prefetched-files}
RAW_FOLDER=${3:-/tmp/raw-files}
BRGM_PROXY=${4:null}

if [[ -n ${BRGM_PROXY} ]]
then
    BRGM_PROXY='-x '${BRGM_PROXY}
fi

echo "`date +"%Y/%m/%d %H:%M:%S"` : Fetching regions from ${SERVER} ..."
mkdir -p ${RAW_FOLDER}
curl ${BRGM_PROXY} -s ${SERVER}/opendata/get-all-region-codes-json.action | jq . > ${OUTPUT}/all-region-codes.json

echo "`date +"%Y/%m/%d %H:%M:%S"` : Now fetch individual regions ..."
mkdir -p ${OUTPUT}/regions
cat ${OUTPUT}/all-region-codes.json | jq -r '.[]' | awk -v srv=${SERVER} -v output=${OUTPUT} -v proxy="${BRGM_PROXY}" '{print "curl "proxy" -sSf \""srv"/opendata/get-region-json.action?code="$1"\" -o "output"/regions/"$1".json"}' | sh
updatedCount=`find ${OUTPUT}/regions -mtime -1 -type f -name "*.json" -exec ls -l  {} + |wc -l`
notUpdatedCount=`find ${OUTPUT}/regions -mtime +1 -type f -name "*.json" -exec ls -l  {} + |wc -l`
echo "`date +"%Y/%m/%d %H:%M:%S"` : ${updatedCount} individual regions updated. (and ${notUpdatedCount} not updated)"

echo "`date +"%Y/%m/%d %H:%M:%S"` : Now fetch individual regions geojson ..."
cat ${OUTPUT}/all-region-codes.json | jq -r '.[]' | awk -v srv=${SERVER} -v output=${OUTPUT} -v proxy="${BRGM_PROXY}" '{print "curl "proxy" -sSf \""srv"/opendata/get-region-geojson.action?code="$1"\" -o "output"/regions/"$1".geojson"}' | sh
updatedCount=`find ${OUTPUT}/regions -mtime -1 -type f -name "*.geojson" -exec ls -l  {} + |wc -l`
notUpdatedCount=`find ${OUTPUT}/regions -mtime +1 -type f -name "*.geojson" -exec ls -l  {} + |wc -l`
echo "`date +"%Y/%m/%d %H:%M:%S"` : ${updatedCount} individual regions geojson updated. (and ${notUpdatedCount} not updated)"

