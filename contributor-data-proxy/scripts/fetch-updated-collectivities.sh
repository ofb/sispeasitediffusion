#!/bin/bash
SERVER=${1:-http://localhost:8080/sispea}
OUTPUT=${2:-prefetched-files}
RAW_FOLDER=${3:-/tmp/raw-files}
BRGM_PROXY=${4:null}

if [[ -n ${BRGM_PROXY} ]]
then
    BRGM_PROXY='-x '${BRGM_PROXY}
fi

echo "`date +"%Y/%m/%d %H:%M:%S"` : Fetching updated collectivities from ${SERVER} ..."
mkdir -p ${RAW_FOLDER}
curl ${BRGM_PROXY} -s ${SERVER}/opendata/get-updated-collectivity-ids-json.action | jq . > ${RAW_FOLDER}/updated-collectivity-ids.json

echo "`date +"%Y/%m/%d %H:%M:%S"` : Now fetch individual updated collectivities ..."
mkdir -p ${OUTPUT}/collectivities
cat ${RAW_FOLDER}/updated-collectivity-ids.json | jq -r '.[]' | awk -v srv=${SERVER} -v output=${OUTPUT} -v proxy="${BRGM_PROXY}" '{print "curl "proxy" -sSf \""srv"/opendata/get-collectivity-json.action?id="$1"\" -o "output"/collectivities/"$1".json"}' | sh

