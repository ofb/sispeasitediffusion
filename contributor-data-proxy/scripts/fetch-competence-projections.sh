#!/bin/bash
SERVER=${1:-http://localhost:8080/sispea}
OUTPUT=${2:-prefetched-files}
RAW_FOLDER=${3:-/tmp/raw-files}
BRGM_PROXY=${4:null}

if [[ -n ${BRGM_PROXY} ]]
then
    BRGM_PROXY='-x '${BRGM_PROXY}
fi

echo "`date +"%Y/%m/%d %H:%M:%S"` : Fetching competence projections from ${SERVER} ..."
mkdir -p ${OUTPUT}/projections

curl ${BRGM_PROXY} -sSf "${SERVER}/opendata/get-competence-projections-json.action?competence=EauPotable" -o ${OUTPUT}/projections/EauPotable.json
curl ${BRGM_PROXY} -sSf "${SERVER}/opendata/get-competence-projections-json.action?competence=AssainissementCollectif" -o ${OUTPUT}/projections/AssainissementCollectif.json
curl ${BRGM_PROXY} -sSf "${SERVER}/opendata/get-competence-projections-json.action?competence=AssainissementNonCollectif" -o ${OUTPUT}/projections/AssainissementNonCollectif.json

updatedCount=`find ${OUTPUT}/projections -mtime -1 -type f -name "*.json" -exec ls -l  {} + |wc -l`
notUpdatedCount=`find ${OUTPUT}/projections -mtime +1 -type f -name "*.json" -exec ls -l  {} + |wc -l`
echo "`date +"%Y/%m/%d %H:%M:%S"` : ${updatedCount} competence projections updated. (and ${notUpdatedCount} not updated)"