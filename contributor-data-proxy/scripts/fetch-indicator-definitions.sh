#!/bin/bash
SERVER=${1:-http://localhost:8080/sispea}
OUTPUT=${2:-prefetched-files}
RAW_FOLDER=${3:-/tmp/raw-files}
BRGM_PROXY=${4:null}

if [[ -n ${BRGM_PROXY} ]]
then
    BRGM_PROXY='-x '${BRGM_PROXY}
fi

echo "`date +"%Y/%m/%d %H:%M:%S"` : Fetching indicator definitions from ${SERVER} ..."
mkdir -p ${OUTPUT}
curl ${BRGM_PROXY} -sSf "${SERVER}/opendata/get-indicator-definitions-json.action"  -o ${OUTPUT}/indicator-definitions.json

