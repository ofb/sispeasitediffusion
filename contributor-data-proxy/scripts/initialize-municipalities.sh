#!/bin/bash
SERVER=${1:-http://localhost:8080/sispea}
OUTPUT=${2:-prefetched-files}
RAW_FOLDER=${3:-/tmp/raw-files}
BRGM_PROXY=${4:null}

if [[ -n ${BRGM_PROXY} ]]
then
    BRGM_PROXY='-x '${BRGM_PROXY}
fi

echo "`date +"%Y/%m/%d %H:%M:%S"` : Fetching municipalities for initialization from ${SERVER} ..."
mkdir -p ${RAW_FOLDER}
curl ${BRGM_PROXY} -s ${SERVER}/opendata/get-all-municipality-insees-json.action | jq . > ${OUTPUT}/all-municipalities-insees.json

echo "`date +"%Y/%m/%d %H:%M:%S"` : Now fetch individual municipalities geojson for initialization ..."
cat ${OUTPUT}/all-municipalities-insees.json | jq -r '.[]' | awk -v srv=${SERVER} -v output=${OUTPUT} -v proxy="${BRGM_PROXY}" '{print "curl "proxy" -sSf \""srv"/opendata/get-municipality-geojson.action?insee="$1"\" -o "output"/municipalities/"$1".geojson"}' | sh

