#!/bin/bash
SERVER=${1:-http://localhost:8080/sispea}
OUTPUT=${2:-prefetched-files}
RAW_FOLDER=${3:-/tmp/raw-files}
BRGM_PROXY=${4:null}

if [[ -n ${BRGM_PROXY} ]]
then
    BRGM_PROXY='-x '${BRGM_PROXY}
fi

echo "`date +"%Y/%m/%d %H:%M:%S"` : Fetching services from ${SERVER} ..."
mkdir -p ${RAW_FOLDER}
curl ${BRGM_PROXY} -s ${SERVER}/opendata/get-all-service-ids-json.action | jq . > ${OUTPUT}/all-service-ids.json

echo "`date +"%Y/%m/%d %H:%M:%S"` : Now fetch individual services ..."
mkdir -p ${OUTPUT}/services
cat ${OUTPUT}/all-service-ids.json | jq -r '.[]' | awk -v srv=${SERVER} -v output=${OUTPUT} -v proxy="${BRGM_PROXY}" '{print "curl "proxy" -sSf \""srv"/opendata/get-service-json.action?id="$1"\" -o "output"/services/"$1".json"}' | sh
updatedCount=`find ${OUTPUT}/services -mtime -1 -type f -name "*.json" -exec ls -l  {} + |wc -l`
notUpdatedCount=`find ${OUTPUT}/services -mtime +1 -type f -name "*.json" -exec ls -l  {} + |wc -l`
echo "`date +"%Y/%m/%d %H:%M:%S"` : ${updatedCount} individual services updated. (and ${notUpdatedCount} not updated)"

echo "`date +"%Y/%m/%d %H:%M:%S"` : Now fetch individual services geojson ..."
cat ${OUTPUT}/all-service-ids.json | jq -r '.[]' | awk -v srv=${SERVER} -v output=${OUTPUT} -v proxy="${BRGM_PROXY}" '{print "curl "proxy" -sSf \""srv"/opendata/get-service-geojson.action?code="$1"\" -o "output"/services/"$1".geojson"}' | sh
updatedCount=`find ${OUTPUT}/services -mtime -1 -type f -name "*.geojson" -exec ls -l  {} + |wc -l`
notUpdatedCount=`find ${OUTPUT}/services -mtime +1 -type f -name "*.geojson" -exec ls -l  {} + |wc -l`
echo "`date +"%Y/%m/%d %H:%M:%S"` : ${updatedCount} individual services geojson updated. (and ${notUpdatedCount} not updated)"
