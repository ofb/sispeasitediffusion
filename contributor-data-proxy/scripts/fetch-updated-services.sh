#!/bin/bash
SERVER=${1:-http://localhost:8080/sispea}
OUTPUT=${2:-prefetched-files}
RAW_FOLDER=${3:-/tmp/raw-files}
BRGM_PROXY=${4:null}

if [[ -n ${BRGM_PROXY} ]]
then
    BRGM_PROXY='-x '${BRGM_PROXY}
fi

echo "`date +"%Y/%m/%d %H:%M:%S"` : Fetching updated services from ${SERVER} ..."
mkdir -p ${RAW_FOLDER}
curl ${BRGM_PROXY} -s ${SERVER}/opendata/get-updated-service-ids-json.action | jq . > ${RAW_FOLDER}/updated-service-ids.json

echo "`date +"%Y/%m/%d %H:%M:%S"` : Now fetch individual updated services ..."
mkdir -p ${OUTPUT}/services
cat ${RAW_FOLDER}/updated-service-ids.json | jq -r '.[]' | awk -v srv=${SERVER} -v output=${OUTPUT} -v proxy="${BRGM_PROXY}" '{print "curl "proxy" -sSf \""srv"/opendata/get-service-json.action?id="$1"\" -o "output"/services/"$1".json"}' | sh

