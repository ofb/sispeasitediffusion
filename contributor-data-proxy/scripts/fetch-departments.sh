#!/bin/bash
SERVER=${1:-http://localhost:8080/sispea}
OUTPUT=${2:-prefetched-files}
RAW_FOLDER=${3:-/tmp/raw-files}
BRGM_PROXY=${4:null}

if [[ -n ${BRGM_PROXY} ]]
then
    BRGM_PROXY='-x '${BRGM_PROXY}
fi

echo "`date +"%Y/%m/%d %H:%M:%S"` : Fetching departments from ${SERVER} ..."
mkdir -p ${RAW_FOLDER}
curl ${BRGM_PROXY} -s ${SERVER}/opendata/get-all-department-codes-json.action | jq . > ${OUTPUT}/all-department-codes.json

echo "`date +"%Y/%m/%d %H:%M:%S"` : Now fetch individual departments ..."
mkdir -p ${OUTPUT}/departments
cat ${OUTPUT}/all-department-codes.json | jq -r '.[]' | awk -v srv=${SERVER} -v output=${OUTPUT} -v proxy="${BRGM_PROXY}" '{print "curl "proxy" -sSf \""srv"/opendata/get-department-json.action?code="$1"\" -o "output"/departments/"$1".json"}' | sh
updatedCount=`find ${OUTPUT}/departments -mtime -1 -type f -name "*.json" -exec ls -l  {} + |wc -l`
notUpdatedCount=`find ${OUTPUT}/departments -mtime +1 -type f -name "*.json" -exec ls -l  {} + |wc -l`
echo "`date +"%Y/%m/%d %H:%M:%S"` : ${updatedCount} individual departments updated. (and ${notUpdatedCount} not updated)"

echo "`date +"%Y/%m/%d %H:%M:%S"` : Now fetch individual departments geojson ..."
cat ${OUTPUT}/all-department-codes.json | jq -r '.[]' | awk -v srv=${SERVER} -v output=${OUTPUT} -v proxy="${BRGM_PROXY}" '{print "curl "proxy" -sSf \""srv"/opendata/get-department-geojson.action?code="$1"\" -o "output"/departments/"$1".geojson"}' | sh
updatedCount=`find ${OUTPUT}/departments -mtime -1 -type f -name "*.geojson" -exec ls -l  {} + |wc -l`
notUpdatedCount=`find ${OUTPUT}/departments -mtime +1 -type f -name "*.geojson" -exec ls -l  {} + |wc -l`
echo "`date +"%Y/%m/%d %H:%M:%S"` : ${updatedCount} individual departments geojson updated. (and ${notUpdatedCount} not updated)"

