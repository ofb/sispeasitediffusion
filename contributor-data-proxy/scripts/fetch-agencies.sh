#!/bin/bash
SERVER=${1:-http://localhost:8080/sispea}
OUTPUT=${2:-prefetched-files}
RAW_FOLDER=${3:-/tmp/raw-files}
BRGM_PROXY=${4:null}

if [[ -n ${BRGM_PROXY} ]]
then
    BRGM_PROXY='-x '${BRGM_PROXY}
fi

echo "`date +"%Y/%m/%d %H:%M:%S"` : Fetching agencies from ${SERVER} ..."
mkdir -p ${RAW_FOLDER}
curl ${BRGM_PROXY} -s ${SERVER}/opendata/get-all-agency-codes-json.action | jq . > ${OUTPUT}/all-agency-codes.json

echo "`date +"%Y/%m/%d %H:%M:%S"` : Now fetch individual agencies ..."
mkdir -p ${OUTPUT}/agencies
cat ${OUTPUT}/all-agency-codes.json | jq -r '.[]' | awk -v srv=${SERVER} -v output=${OUTPUT} -v proxy="${BRGM_PROXY}" '{print "curl "proxy" -sSf \""srv"/opendata/get-agency-json.action?code="$1"\" -o "output"/agencies/"$1".json"}' | sh
updatedCount=`find ${OUTPUT}/agencies -mtime -1 -type f -name "*.json" -exec ls -l  {} + |wc -l`
notUpdatedCount=`find ${OUTPUT}/agencies -mtime +1 -type f -name "*.json" -exec ls -l  {} + |wc -l`
echo "`date +"%Y/%m/%d %H:%M:%S"` : ${updatedCount} individual agencies updated. (and ${notUpdatedCount} not updated)"
