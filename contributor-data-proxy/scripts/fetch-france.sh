#!/bin/bash
SERVER=${1:-http://localhost:8080/sispea}
OUTPUT=${2:-prefetched-files}
RAW_FOLDER=${3:-/tmp/raw-files}
BRGM_PROXY=${4:null}

if [[ -n ${BRGM_PROXY} ]]
then
    BRGM_PROXY='-x '${BRGM_PROXY}
fi

echo "`date +"%Y/%m/%d %H:%M:%S"` : Fetching France from ${SERVER} ..."
mkdir -p ${OUTPUT}
curl ${BRGM_PROXY} -sSf "${SERVER}/opendata/get-france-json.action" -o ${OUTPUT}/france.json

echo "`date +"%Y/%m/%d %H:%M:%S"` : Fetching France geojson from ${SERVER} ..."
curl ${BRGM_PROXY} -sSf "${SERVER}/opendata/get-france-geojson.action" -o ${OUTPUT}/france.geojson

