#!/bin/bash
SERVER=${1:-http://localhost:8080/sispea}
OUTPUT=${2:-prefetched-files}
BRGM_PROXY=${4:null}

if [[ -n ${BRGM_PROXY} ]]
then
    BRGM_PROXY='-x '${BRGM_PROXY}
fi

mkdir -p ${OUTPUT}/rss
echo "`date +"%Y/%m/%d %H:%M:%S"` : Fetching rss feeds from ${SERVER} ..."
curl ${BRGM_PROXY} -sSf "https://www.oieau.fr/eaudanslaville/rss/actualites"  -o ${OUTPUT}/rss/oieau.rss

