#!/bin/bash
SERVER=${1:-http://localhost:8080/sispea}
OUTPUT=${2:-prefetched-files}
RAW_FOLDER=${3:-/tmp/raw-files}
BRGM_PROXY=${4:null}

if [[ -n ${BRGM_PROXY} ]]
then
    BRGM_PROXY='-x '${BRGM_PROXY}
fi

echo "`date +"%Y/%m/%d %H:%M:%S"` : Fetching collectivities from ${SERVER} ..."
mkdir -p ${RAW_FOLDER}
curl ${BRGM_PROXY} -s ${SERVER}/opendata/get-all-collectivity-ids-json.action | jq . > ${OUTPUT}/all-collectivity-ids.json

echo "`date +"%Y/%m/%d %H:%M:%S"` : Now fetch individual collectivities ..."
mkdir -p ${OUTPUT}/collectivities
cat ${OUTPUT}/all-collectivity-ids.json | jq -r '.[]' | awk -v srv=${SERVER} -v output=${OUTPUT} -v proxy="${BRGM_PROXY}" '{print "curl "proxy" -sSf \""srv"/opendata/get-collectivity-json.action?id="$1"\" -o "output"/collectivities/"$1".json"}' | sh
updatedCount=`find ${OUTPUT}/collectivities -mtime -1 -type f -name "*.json" -exec ls -l  {} + |wc -l`
notUpdatedCount=`find ${OUTPUT}/collectivities -mtime +1 -type f -name "*.json" -exec ls -l  {} + |wc -l`
echo "`date +"%Y/%m/%d %H:%M:%S"` : ${updatedCount} individual collectivities updated. (and ${notUpdatedCount} not updated)"

echo "`date +"%Y/%m/%d %H:%M:%S"` : Now fetch individual collectivities geojson ..."
cat ${OUTPUT}/all-collectivity-ids.json | jq -r '.[]' | awk -v srv=${SERVER} -v output=${OUTPUT} -v proxy="${BRGM_PROXY}" '{print "curl "proxy" -sSf \""srv"/opendata/get-collectivity-geojson.action?code="$1"\" -o "output"/collectivities/"$1".geojson"}' | sh
updatedCount=`find ${OUTPUT}/collectivities -mtime -1 -type f -name "*.geojson" -exec ls -l  {} + |wc -l`
notUpdatedCount=`find ${OUTPUT}/collectivities -mtime +1 -type f -name "*.geojson" -exec ls -l  {} + |wc -l`
echo "`date +"%Y/%m/%d %H:%M:%S"` : ${updatedCount} individual collectivities geojson updated. (and ${notUpdatedCount} not updated)"
