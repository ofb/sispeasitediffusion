#!/bin/bash
SERVER=${1:-http://localhost:8080/sispea}
OUTPUT=${2:-prefetched-files}
RAW_FOLDER=${3:-/tmp/raw-files}
BRGM_PROXY=${4:null}

if [[ -n ${BRGM_PROXY} ]]
then
    BRGM_PROXY='-x '${BRGM_PROXY}
fi

mkdir -p ${OUTPUT}/documents
echo "`date +"%Y/%m/%d %H:%M:%S"` : Fetching yearly extractions files information from ${SERVER} ..."
curl ${BRGM_PROXY} -sSf "${SERVER}/opendata/get-yearly-extraction-files-json.action"  -o ${OUTPUT}/documents/yearly-extraction-files.json

echo "`date +"%Y/%m/%d %H:%M:%S"` : Now fetch individual yearly extractions file ..."
mkdir -p ${OUTPUT}/documents/openData
cat ${OUTPUT}/documents/yearly-extraction-files.json | jq -r '.[] | .name' | awk -v srv=${SERVER} -v output=${OUTPUT} -v proxy="${BRGM_PROXY}" '{print "curl "proxy" -sSf \""srv"/opendata/get-open-data-file.action?folder=openData&fileName="$1"\" -o "output"/documents/openData/"$1""}' | sh

echo "`date +"%Y/%m/%d %H:%M:%S"` : Fetching service members composition files information from ${SERVER} ..."
curl ${BRGM_PROXY} -sSf "${SERVER}/opendata/get-service-members-composition-files-json.action"  -o ${OUTPUT}/documents/service-members-composition-files.json

echo "`date +"%Y/%m/%d %H:%M:%S"` : Now fetch individual service members composition file ..."
mkdir -p ${OUTPUT}/documents/serviceMembers
cat ${OUTPUT}/documents/service-members-composition-files.json | jq -r '.[] | .name' | awk -v srv=${SERVER} -v output=${OUTPUT} -v proxy="${BRGM_PROXY}" '{print "curl "proxy" -sSf \""srv"/opendata/get-open-data-file.action?folder=serviceMembers&fileName="$1"\" -o "output"/documents/serviceMembers/"$1""}' | sh

echo "`date +"%Y/%m/%d %H:%M:%S"` : Fetching collectivity description files information from ${SERVER} ..."
curl ${BRGM_PROXY} -sSf "${SERVER}/opendata/get-collectivity-description-files-json.action"  -o ${OUTPUT}/documents/collectivity-description-files.json

echo "`date +"%Y/%m/%d %H:%M:%S"` : Now fetch individual collectivity description files file ..."
mkdir -p ${OUTPUT}/documents/collectivityDescriptions
cat ${OUTPUT}/documents/collectivity-description-files.json | jq -r '.[] | .name' | awk -v srv=${SERVER} -v output=${OUTPUT} -v proxy="${BRGM_PROXY}" '{print "curl "proxy" -sSf \""srv"/opendata/get-open-data-file.action?folder=collectivityDescriptions&fileName="$1"\" -o "output"/documents/collectivityDescriptions/"$1""}' | sh

echo "`date +"%Y/%m/%d %H:%M:%S"` : Fetching tarifs description files information from ${SERVER} ..."
curl ${BRGM_PROXY} -sSf "${SERVER}/opendata/get-tarifs-description-files-json.action"  -o ${OUTPUT}/documents/tarifs-description-files.json

echo "`date +"%Y/%m/%d %H:%M:%S"` : Now fetch individual tarif description files file ..."
mkdir -p ${OUTPUT}/documents/tarifsDescriptions
cat ${OUTPUT}/documents/tarifs-description-files.json | jq -r '.[] | .name' | awk -v srv=${SERVER} -v output=${OUTPUT} -v proxy="${BRGM_PROXY}" '{print "curl "proxy" -sSf \""srv"/opendata/get-open-data-file.action?folder=tarifsDescriptions&fileName="$1"\" -o "output"/documents/tarifsDescriptions/"$1""}' | sh

echo "`date +"%Y/%m/%d %H:%M:%S"` : Fetching indicator codes from ${SERVER} ..."
curl ${BRGM_PROXY} -sSf "${SERVER}/opendata/get-all-indicator-codes-json.action"  -o ${OUTPUT}/documents/all-indicator-codes.json

echo "`date +"%Y/%m/%d %H:%M:%S"` : Now fetch individual cards ..."
mkdir -p ${OUTPUT}/documents/indicators
cat ${OUTPUT}/documents/all-indicator-codes.json | jq -r '.[]' | awk -v srv=${SERVER} -v output=${OUTPUT} -v proxy="${BRGM_PROXY}" '{print "curl "proxy" -sSf \""srv"/opendata/get-indicator-definition-pdf-card.action?code="$1"\" -o "output"/documents/indicators/"$1".pdf"}' | sh

echo "`date +"%Y/%m/%d %H:%M:%S"` : Fetching bilan files information from ${SERVER} ..."
curl ${BRGM_PROXY} -sSf "${SERVER}/opendata/get-bilan-files-json.action"  -o ${OUTPUT}/documents/bilan-files.json

echo "`date +"%Y/%m/%d %H:%M:%S"` : Now fetch individual bilan files file ..."
mkdir -p ${OUTPUT}/documents/bilans
cat ${OUTPUT}/documents/bilan-files.json | jq -r '.[] | .name' | awk -v srv=${SERVER} -v output=${OUTPUT} -v proxy="${BRGM_PROXY}" '{print "curl "proxy" -sSf \""srv"/opendata/get-open-data-file.action?folder=bilans&fileName="$1"\" -o "output"/documents/bilans/"$1""}' | sh

