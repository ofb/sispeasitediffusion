#!/bin/bash
TOKEN=${1}
SERVER=${2:-http://sispea.brgm-rec.fr/sispea}
echo "Fetching municipalities from ${SERVER} with token: ${TOKEN} ..."
mkdir -p raw-data
curl -s -d "paginationParameterJson=%7B%22pageNumber%22%3A0%2C%22pageSize%22%3A-1%7D&filterJson=%7B%22name%22%3A%22%22%7D" --header "X-Sispea-Token: ${TOKEN}" -X POST ${SERVER}/search/advanced-search-municipalities-json.action | jq . > raw-data/municipalities.json
echo "Now split into individual files ..."
rm -rf prefetched-files/municipalities/*.json
mkdir -p prefetched-files/municipalities
jq -cr '.elements[] | [.municipalityId, .]' raw-data/municipalities.json | cut -c 2- | sed 's/.$//' | sed 's/,{/#{/g' | awk -F '#' '{print $2 > "prefetched-files/municipalities/"$1".json"}'

#Pour générer un fichier d'index des identifiants :
#jq '{"ids":[.elements[].municipalityId]}' raw-data/municipalities.json > prefetched-files/municipalities/index.json

