'use strict'

async function up(knex) {
    // Load Extensions
    await knex.raw('CREATE EXTENSION IF NOT EXISTS "unaccent"');

}

module.exports = { up };
