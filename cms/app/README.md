# Backend CMS (Strapi)

Le backend CMS est destiné aux contenus éditoriaux de Sispea Diffusion (Articles, actualités, événements, glossaire, pages d'informations).

`cd src/plugins/wysiwyg-ckeditor-5 && /opt/app/src/plugins/wysiwyg-ckeditor-5 && yarn install && yarn build`

## Démarrer le backend en développement

`yarn develop` démarre l'application (sur le port 1337 par défaut).

Dans ce cas, l'**autoReload** est activé.
Strapi détecte alors les modifications nécessitant un redémarrage de l'application pour être prise en compte.
C'est le cas des modifications du modèle de données, car des fichiers sont générés et la base de données est modifiée au démarrage.

## Démarrer le backend en production

* `yarn build`
* `yarn start`

L'**autoReload** n'étant pas activé, il est nécessaire de redémarrer l'application pour prendre en compte certains changements.
