// path: ./src/plugins/wysiwyg/admin/src/components/Editor/index.js

import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import CustomClassicEditor from "./build/ckeditor";
import styles from "./styles";

import { Box } from "@strapi/design-system/Box";

const Wrapper = styled(Box)`
  ${styles}
`;


const configuration = {
  toolbar: [
    'heading',
    {
        label: 'Mise en forme du texte',
        icon: 'text',
        items: [
          'bold',
          'italic',
          'underline',
          'strikethrough',
          'fontColor',
          'fontBackgroundColor',
          'fontFamily',
          'fontSize',
          'subscript',
          'superscript',
          'removeFormat'
        ]
    },
    '|',
    'alignment',
    'outdent',
    'indent',
    '|',
    'bulletedList',
    'numberedList',
    '|',
    'link',
    'mediaEmbed',
    'insertTable',
    'HtmlEmbed',
    'horizontalLine',
    'sourceEditing',
    '|',
    'undo',
    'redo'
  ],
};

const Editor = ({ onChange, name, value, disabled }) => {
  return (
    <Wrapper>
      <CKEditor
        editor={CustomClassicEditor}
        disabled={disabled}
        config={configuration}
        data={value || ""}
        onReady={(editor) => editor.setData(value || "")}
        onChange={(event, editor) => {
          const data = editor.getData();
          onChange({ target: { name, value: data } });
        }}
      />
    </Wrapper>
  );
};

Editor.defaultProps = {
  value: "",
  disabled: false,
};

Editor.propTypes = {
  onChange: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  value: PropTypes.string,
  disabled: PropTypes.bool,
};

export default Editor;
