const styles = `
.ck-editor__main {
    color: #415462;
    min-height: ${200 / 16}em;
    > div {
      min-height: ${200 / 16}em;
    }

    // Since Strapi resets css styles, it can be configured here (h2, h3, strong, i, ...)
    strong {
      font-weight: bold;
    }
    i {
      font-style: italic;
    }
    h1, h2, h3, h4 {
      font-weight: bold;
    }
    h1 { font-size: 28px; }
    h2 {
      font-size: 28px;
      color: rgb(36, 51, 62);
      margin-bottom: 42px;
    }
    h3 {
      font-size: 20px;
      color: rgb(44, 61, 73);
      margin-bottom: 36px;
    }
    h4 {
      font-size: 20px;
      color: rgb(55, 73, 86);
      margin-bottom: 29px;

    }

    sup {
      vertical-align: super;
      font-size: smaller;
    }

    sub {
      vertical-align: sub;
      font-size: smaller;
    }

    p {
      font-size: 1em;
      line-height: 1.6em;
      padding-top: 0.2em;
      margin-bottom: 0.8em;
    }

    ul, ol {
        list-style: initial;
        margin-left: 0px;
        padding-left: 50px;
        margin-bottom: 24px;
    }

    li {
        margin-bottom: 6px;
        line-height: 24px;
    }
    
    ol li {
        list-style: decimal;
    }
    
    ul li {
        list-style: square;
    }

    figure figcaption {
      padding: 10px 20px;
      background: #f5f5f5;
      color: #555;
      margin-top: 10px;
    }
  }
`;
export default styles