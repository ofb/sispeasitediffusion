'use strict';

module.exports = {
  index(ctx) {
    ctx.body = strapi
      .plugin('wysiwyg-ckeditor-5')
      .service('myService')
      .getWelcomeMessage();
  },
};
