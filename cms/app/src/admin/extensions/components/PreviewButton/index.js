import React from 'react';
import { Button } from '@strapi/design-system/Button';
import Eye from '@strapi/icons/Eye';
import { useCMEditViewDataManager } from '@strapi/helper-plugin';
import { useIntl } from 'react-intl';

const PreviewButton = () => {
  const { formatMessage } = useIntl();
  const { modifiedData, layout } = useCMEditViewDataManager();

  const typesWithPreviewMode = {
    'accueil-encart' : '',
    'accueil-liens' : '',
    'actualite' : 'actualite/' + modifiedData.id,
    'article' : modifiedData.identifiant,
    'chiffres-cle' : 'chiffres/' + modifiedData.id,
    'glossaire' : 'glossaire/',
    'loi-sapin-introduction' : 'mediatheque/loi-sapin',
    'menu-principal' : 'menu',
    'page' : modifiedData.identifiant,
    'professionnel-encart' : 'pro',
    'professionnel-liens' : 'pro',
    'rapports-introduction' : 'mediatheque/rapports',
  };

  const typesWithoutDraftPreviewMode = [
    'accueil-liens',
    'glossaire',
    'loi-sapin-introduction',
    'menu-principal',
    'professionnel-liens'
  ];


  const previewUrl = WEBAPP_ROOT_URL + '/',
        previewMode = modifiedData.publishedAt || typesWithoutDraftPreviewMode.includes(layout.apiID) ? '' : '?preview';

  let previewUri = null;

  if (typesWithPreviewMode[layout.apiID] != undefined) {
    previewUri = typesWithPreviewMode[layout.apiID];
  } else {
    return null;
  }

  const handlePreview = () => {
    window.open(previewUrl + previewUri + previewMode, '_blank').focus();
  };

  const content = previewMode && !typesWithoutDraftPreviewMode.includes(layout.apiID)  ?
    {
      id: 'components.PreviewButton.preview-draft',
      defaultMessage: 'Preview',
    } :
    {
      id: 'components.PreviewButton.preview-live',
      defaultMessage: 'Preview',
    };

  return (
    <>
      <Button variant="secondary" startIcon={<Eye />} onClick={handlePreview}>
        {formatMessage(content)}
      </Button>
    </>
  );
};

export default PreviewButton;