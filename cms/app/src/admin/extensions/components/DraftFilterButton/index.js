import React from "react";
import { SimpleMenu, MenuItem } from '@strapi/design-system/SimpleMenu';
import { useQueryParams } from "@strapi/helper-plugin";
import { useIntl } from "react-intl";
import { useLocation } from "react-router-dom";
import { useState } from 'react';

const ReviewContent = () => {
  const { formatMessage } = useIntl();
  const [{ query }, setQuery] = useQueryParams();
  const filters = query.filters?.$and || [];
  const publishedAtFilter = filters.find((filter) => filter.publishedAt);

  const validPathNames = [
    "/content-manager/collectionType/api::aide.aide",
    "/content-manager/collectionType/api::article.article",
    "/content-manager/collectionType/api::actualite.actualite",
    "/content-manager/collectionType/api::chiffres-cle.chiffres-cle",
    "/content-manager/collectionType/api::entree-de-menu.entree-de-menu",
    "/content-manager/collectionType/api::faq-question.faq-question",
    "/content-manager/collectionType/api::glossaire.glossaire",
    "/content-manager/collectionType/api::page.page",
    "/content-manager/collectionType/api::publication.publication"
  ];
  const displayButton = validPathNames.includes(useLocation().pathname);

  const resetPublishFilter = () => {
    const nextFilters = filters.filter((filter) => {
      const [filterName] = Object.keys(filter);
      return filterName !== "publishedAt";
    });
    setQuery({ filters: { $and: nextFilters }, page: 1 });
  }

  const addPublishFilter = (value) => {
    const nextFilters = filters.slice(),
          filterValue = value == 'drafts' ? { $null: "true" } : { $notNull: "true" };

    nextFilters.push({
      publishedAt: filterValue,
    });

    setQuery({ filters: { $and: nextFilters }, page: 1 });
  }

  const changePublishFilter = (value) => {
    publishedAtFilter.publishedAt = value == 'drafts' ? { $null: "true" } : { $notNull: "true" };
    setQuery({ filters: { $and: filters }, page: 1 });
  }

  const update = (value) => {
    setValue(value);

    if (publishedAtFilter != undefined) {
      if ('all' == value) {
        resetPublishFilter();
      } else {
        changePublishFilter(value)
      }
    } else {
      if ('all' != value) {
        addPublishFilter(value);
      }
    }
  }

  const [val, setValue] = useState('all');

  const showOnlyDraftsOption = {
      id: "components.DraftFilterButton.show-drafts",
      defaultMessage: "Show drafts",
    },
    showOnlyPublishedOption = {
      id: "components.DraftFilterButton.show-published",
      defaultMessage: "Show published",
    },
    showAllItemsOption = {
      id: "components.DraftFilterButton.show-all",
      defaultMessage: "Show all",
    };


  let selectLabel = publishedAtFilter == undefined ?
      formatMessage(showAllItemsOption) : publishedAtFilter.publishedAt.$null ?
        formatMessage(showOnlyDraftsOption) :
        formatMessage(showOnlyPublishedOption);

  if (displayButton) {
    return (
      <SimpleMenu id="label" label={selectLabel}>
        <MenuItem id="drafts" onClick={() => update('drafts')}>
          {formatMessage(showOnlyDraftsOption)}
        </MenuItem>
        <MenuItem id="published" onClick={() => update('published')}>
          {formatMessage(showOnlyPublishedOption)}
        </MenuItem>
        <MenuItem id="all" onClick={() => update('all')}>
          {formatMessage(showAllItemsOption)}
        </MenuItem>
      </SimpleMenu>
    );
  }
  return <></>;
};

export default ReviewContent;
