import React from 'react';
import { Button } from '@strapi/design-system/Button';
import Clock from '@strapi/icons/Clock';
import { useCMEditViewDataManager } from '@strapi/helper-plugin';
import { useIntl } from 'react-intl';

const HistoryButton = () => {
  const { formatMessage } = useIntl();
  const { modifiedData, layout } = useCMEditViewDataManager();

  const typesWithHistoryMode = {
    'accueil-encart' : 'Page d\'accueil - Encart',
    'accueil-liens' : 'Page d\'accueil - Barre de liens',
    'actualite' : 'Actualité',
    'aide' : 'Page d\'aide pour Contributeur',
    'article' : 'Article',
    'chiffres-cle' : 'Chiffre clé',
    'faq-question' : 'FAQ',
    'glossaire' : 'Glossaire',
    'liens-vers-sites-ofb' : 'Lien vers site OFB',
    'loi-sapin-introduction' : 'Loi Sapin - Texte d\'introduction',
    'menu-en-pied-de-page' : 'Menu en pied de page',
    'menu-principal' : 'Menu principal',
    'page' : 'Page',
    'professionnel-encart' : 'Professionnel - Encart',
    'professionnel-liens' : 'Professionnel - Barre de liens',
    'publication' : 'Médiathèque',
    'rapports-introduction' : 'Rapports - Texte d\'introduction',
  };

  if (typesWithHistoryMode[layout.apiID] == undefined) {
    return null;
  }

  const goToHistory = () => {
    
    let url = CMS_STRAPI_ADMIN_URL ? CMS_STRAPI_ADMIN_URL : '/admin';
    url += '/content-manager/collectionType/api::historique-modifications.historique-modifications?page=1&pageSize=10&sort=createdAt:DESC' +
           '&filters[$and][0][contentType][$eq]=' + typesWithHistoryMode[layout.apiID] +
           '&filters[$and][1][contentId][$eq]=' + modifiedData.id;
    window.open(url, '_blank').focus();
  };

  const content = {
      id: 'components.HistoryButton.history',
      defaultMessage: 'Historique',
    };

  return (
    <>
      <Button variant="secondary" startIcon={<Clock />} onClick={goToHistory}>
        {formatMessage(content)}
      </Button>
    </>
  );
};

export default HistoryButton;