import DraftFilterButton from "./extensions/components/DraftFilterButton";
import PreviewButton from "./extensions/components/PreviewButton";
import HistoryButton from "./extensions/components/HistoryButton";

export default {
  config: {
    notifications: {
      release: false
    },
    locales: [
      'fr',
    ],
    translations: {
      fr: {
        "components.DraftFilterButton.show-all": "Voir toutes les entrées",
        "components.DraftFilterButton.show-drafts": "Voir uniquement les brouillons",
        "components.DraftFilterButton.show-published": "Voir uniquement les entrées publiées",
        "components.PreviewButton.preview-draft": "Prévisualiser le brouillon",
        "components.PreviewButton.preview-live": "Voir sur le site",
        "components.HistoryButton.history": "Voir l'historique",
      },
      en: {
        "components.DraftFilterButton.show-all": "Show only drafts",
        "components.DraftFilterButton.show-drafts": "Show only drafts",
        "components.DraftFilterButton.show-published": "Show only published",
        "components.PreviewButton.preview-draft": "Preview draft",
        "components.PreviewButton.preview-live": "View on website",
        "components.HistoryButton.history": "View history",
      },
    },
  },
  bootstrap(app) {
    app.injectContentManagerComponent("listView", "actions", {
      name: "DraftFilterButton",
      Component: DraftFilterButton,
    });
    app.injectContentManagerComponent("editView", "right-links", {
      name: "PreviewButton",
      Component: PreviewButton,
    });
    app.injectContentManagerComponent("editView", "right-links", {
      name: "HistoryButton",
      Component: HistoryButton,
    });
  },
};
