'use strict';

/* eslint-disable no-unused-vars */
module.exports = (config, webpack) => {
  // Note: we provide webpack above so you should not `require` it
  // Perform customizations to webpack config
  // Important: return the modified config

  config.plugins.push(
    new webpack.DefinePlugin({
      CMS_STRAPI_ADMIN_URL: JSON.stringify(process.env.CMS_STRAPI_ADMIN_URL),
      WEBAPP_ROOT_URL: JSON.stringify(process.env.WEBAPP_ROOT_URL),
    })
  )

  return config;
};