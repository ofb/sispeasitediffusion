'use strict';

/**
 * accueil-liens router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::accueil-liens.accueil-liens');
