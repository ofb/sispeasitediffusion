'use strict';

/**
 * accueil-liens service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::accueil-liens.accueil-liens');
