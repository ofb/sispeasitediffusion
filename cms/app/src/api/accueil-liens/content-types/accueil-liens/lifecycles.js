module.exports = {

  afterCreate(event) {
    const { result, params } = event;

    strapi.entityService.create('api::historique-modifications.historique-modifications', {
      data : {
        contentId: result.id,
        contentType: 'Page d\'accueil - Barre de liens',
        contentTitle: "Barre de liens de la page d'accueil",
        modificationType: 'Création',
        author: result.createdBy.firstname + " " + result.createdBy.lastname
      }
    });
  },

  afterUpdate(event) {
    const { result, params } = event;

    strapi.entityService.create('api::historique-modifications.historique-modifications', {
      data : {
        contentId: result.id,
        contentType: 'Page d\'accueil - Barre de liens',
        contentTitle: "Barre de liens de la page d'accueil",
        modificationType: 'Mise à jour',
        author: result.updatedBy.firstname + " " + result.updatedBy.lastname
      }
    });
  },
};