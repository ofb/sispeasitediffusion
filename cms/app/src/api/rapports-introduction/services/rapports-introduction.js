'use strict';

/**
 * rapports-introduction service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::rapports-introduction.rapports-introduction');
