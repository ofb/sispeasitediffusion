'use strict';

/**
 * rapports-introduction router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::rapports-introduction.rapports-introduction');
