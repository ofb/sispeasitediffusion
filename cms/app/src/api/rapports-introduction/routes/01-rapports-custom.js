module.exports = {
  routes: [
    {
      method: 'GET',
      path: '/rapports-introduction/preview',
      handler: 'rapports-introduction.findDraft',
    },
  ]
}
