'use strict';

/**
 *  rapports-introduction controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::rapports-introduction.rapports-introduction', ({ strapi }) => ({

    async findDraft(ctx) {
      const entity = await strapi.db.query('api::rapports-introduction.rapports-introduction').findOne({
          where: {
            publishedAt: null,
          }
        });
      const sanitizedEntity = await this.sanitizeOutput(entity, ctx);
      return this.transformResponse(sanitizedEntity);
    }
  }));
