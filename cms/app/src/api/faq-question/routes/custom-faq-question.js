module.exports = {
    routes: [
      {
        method: "GET",
        path: "/faq-question/search",
        handler: "faq-question.searchQuestions",
      },
    ],
  };