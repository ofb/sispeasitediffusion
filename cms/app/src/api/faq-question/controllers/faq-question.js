'use strict';

/**
 *  faq-question controller
 */

const { createCoreController } = require("@strapi/strapi").factories;
 
module.exports = createCoreController("api::faq-question.faq-question", ({ strapi }) => ({
  async searchQuestions(ctx) {
    const { term } = ctx.request.query;
    const entries = await strapi.db.connection.raw("SELECT * FROM components_faq_faq_questions where unaccent(question) ~ unaccent('" + term + "') or unaccent(reponse) ~ unaccent('" + term + "')" );
    ctx.body = entries.rows;
  },
}));
