// src/api/hello/routes/custom-publications.js

module.exports = {
    routes: [
      {
        method: "GET",
        path: "/publications/years",
        handler: "publication.findDistinctYear",
      },
    ],
  };