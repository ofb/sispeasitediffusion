'use strict';

/**
 *  publication controller
 */

 const { createCoreController } = require("@strapi/strapi").factories;
 
 module.exports = createCoreController("api::publication.publication", ({ strapi }) => ({
   async findDistinctYear(ctx) {
     const entries = await strapi.db.connection.raw("SELECT distinct extract(year from date_publication) FROM publications ORDER BY extract(year from date_publication) desc");
     ctx.body = entries.rows;
   },
 }));