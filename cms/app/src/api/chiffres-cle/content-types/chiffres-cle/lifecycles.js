module.exports = {

  afterCreate(event) {
    const { result, params } = event;

    strapi.entityService.create('api::historique-modifications.historique-modifications', {
      data : {
        contentId: result.id,
        contentType: 'Chiffre clé',
        contentTitle: params.data.titre,
        modificationType: 'Création',
        contentDescription: params.data.description,
        author: result.createdBy.firstname + " " + result.createdBy.lastname
      }
    });
  },

  afterUpdate(event) {
    const { result, params } = event,
          modificationType = params.data.publishedAt === undefined ? 'Mise à jour' :
                             params.data.publishedAt === null ? 'Dépublication' : 'Publication';

    strapi.entityService.create('api::historique-modifications.historique-modifications', {
      data : {
        contentId: result.id,
        contentType: 'Chiffre clé',
        contentTitle: result.titre,
        modificationType: modificationType,
        contentDescription: result.description,
        author: result.updatedBy.firstname + " " + result.updatedBy.lastname
      }
    });
  },
};