'use strict';

/**
 * chiffres-cle router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::chiffres-cle.chiffres-cle');
