module.exports = {
  routes: [
    {
      method: 'GET',
      path: '/chiffres-cles/:id',
      handler: 'chiffres-cle.findOne',
    },
    {
      method: 'GET',
      path: '/chiffres-cles/preview/:id',
      handler: 'chiffres-cle.findOneDraft',
    },
  ]
}
