'use strict';

/**
 * chiffres-cle service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::chiffres-cle.chiffres-cle');
