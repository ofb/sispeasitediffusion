'use strict';

/**
 *  professionnel-encart controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::professionnel-encart.professionnel-encart', ({ strapi }) => ({

  async findDraft(ctx) {
    const entity = await strapi.db.query('api::professionnel-encart.professionnel-encart').findOne({
        where: {
          publishedAt: null,
        }
      });
    const sanitizedEntity = await this.sanitizeOutput(entity, ctx);
    return this.transformResponse(sanitizedEntity);
  }

}))

