'use strict';

/**
 * professionnel-encart service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::professionnel-encart.professionnel-encart');
