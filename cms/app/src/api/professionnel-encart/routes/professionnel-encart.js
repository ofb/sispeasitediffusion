'use strict';

/**
 * professionnel-encart router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::professionnel-encart.professionnel-encart');
