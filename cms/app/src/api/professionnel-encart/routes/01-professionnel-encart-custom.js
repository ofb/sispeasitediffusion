module.exports = {
  routes: [
    {
      method: 'GET',
      path: '/professionnel-encart/preview',
      handler: 'professionnel-encart.findDraft',
    },
  ]
}
