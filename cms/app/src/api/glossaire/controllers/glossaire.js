'use strict';

/**
 *  glossaire controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::glossaire.glossaire', ({ strapi }) => ({
  async findAndSort(ctx) {
    const accents = 'ÀÁÂÃÄÅàáâãäåÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž',
          accentsReplacement = "AAAAAAaaaaaaOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz";
    let letters  = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
                    'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                    'Y', 'Z'],
        glossaireSortedByLetter = [];

    letters.forEach((letter) => {
      glossaireSortedByLetter[letter] = {letter, definitions: []};
    })

    let data = await  strapi.entityService.findMany('api::glossaire.glossaire', {
      sort: { libelle: 'DESC' },
    });

    let glossaireSorted = data.reduce((r, entry) => {
      let firstLetter = entry.libelle[0].toUpperCase();

      if (accents.indexOf(firstLetter) != -1) {
        firstLetter = accentsReplacement[accents.indexOf(firstLetter)]
      }

      glossaireSortedByLetter[firstLetter].definitions.unshift({
        'libelle' :  entry.libelle,
        'definition': entry.definition,
        'source': entry.source
      });

      return glossaireSortedByLetter;
    }, {})
    return Object.values(glossaireSorted);
  },
}))
