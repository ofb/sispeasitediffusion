module.exports = {

  afterCreate(event) {
    const { result, params } = event;

    strapi.entityService.create('api::historique-modifications.historique-modifications', {
      data : {
        contentId: result.id,
        contentType: 'Glossaire',
        contentTitle: params.data.libelle,
        modificationType: 'Création',
        contentDescription: params.data.definition,
        author: result.createdBy.firstname + " " + result.createdBy.lastname
      }
    });
  },

  afterUpdate(event) {
    const { result, params } = event,
          modificationType = params.data.publishedAt === undefined ? 'Mise à jour' :
                             params.data.publishedAt === null ? 'Dépublication' : 'Publication';

    strapi.entityService.create('api::historique-modifications.historique-modifications', {
      data : {
        contentId: result.id,
        contentType: 'Glossaire',
        contentTitle: result.libelle,
        modificationType: modificationType,
        contentDescription: result.definition,
        author: result.updatedBy.firstname + " " + result.updatedBy.lastname
      }
    });
  },
};