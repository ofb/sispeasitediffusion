module.exports = {
  routes: [
    {
      method: 'GET',
      path: '/glossaire',
      handler: 'glossaire.findAndSort',
    }
  ]
}
