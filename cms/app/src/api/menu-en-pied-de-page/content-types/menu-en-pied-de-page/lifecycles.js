module.exports = {

  afterCreate(event) {
    const { result, params } = event;

    strapi.entityService.create('api::historique-modifications.historique-modifications', {
      data : {
        contentId: result.id,
        contentType: 'Menu en pied de page',
        modificationType: 'Création',
        contentDescription: params.data.description,
        author: result.createdBy.firstname + " " + result.createdBy.lastname
      }
    });
  },

  afterUpdate(event) {
    const { result, params } = event;

    strapi.entityService.create('api::historique-modifications.historique-modifications', {
      data : {
        contentId: result.id,
        contentType: 'Menu en pied de page',
        modificationType: 'Mise à jour' ,
        author: result.updatedBy.firstname + " " + result.updatedBy.lastname
      }
    });
  },
};