'use strict';

/**
 * menu-en-pied-de-page service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::menu-en-pied-de-page.menu-en-pied-de-page');
