'use strict';

/**
 * menu-en-pied-de-page router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::menu-en-pied-de-page.menu-en-pied-de-page');
