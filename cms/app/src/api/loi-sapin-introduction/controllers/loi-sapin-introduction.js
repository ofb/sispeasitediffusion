'use strict';

/**
 *  loi-sapin-introduction controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::loi-sapin-introduction.loi-sapin-introduction', ({ strapi }) => ({

    async findDraft(ctx) {
      const entity = await strapi.db.query('api::loi-sapin-introduction.loi-sapin-introduction').findOne({
          where: {
            publishedAt: null,
          }
        });
      const sanitizedEntity = await this.sanitizeOutput(entity, ctx);
      return this.transformResponse(sanitizedEntity);
    }
  }));
