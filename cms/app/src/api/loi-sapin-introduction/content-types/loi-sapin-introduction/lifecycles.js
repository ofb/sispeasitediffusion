module.exports = {

  afterCreate(event) {
    const { result, params } = event;

    strapi.entityService.create('api::historique-modifications.historique-modifications', {
      data : {
        contentId: result.id,
        contentType: 'Loi Sapin - Texte d\'introduction',
        contentTitle: 'Loi Sapin - Texte d\'introduction',
        modificationType: 'Création',
        contentDescription: params.data.introduction,
        author: result.createdBy.firstname + " " + result.createdBy.lastname
      }
    });
  },

  afterUpdate(event) {
    const { result, params } = event,
          modificationType = params.data.publishedAt === undefined ? 'Mise à jour' :
                             params.data.publishedAt === null ? 'Dépublication' : 'Publication';

    strapi.entityService.create('api::historique-modifications.historique-modifications', {
      data : {
        contentId: result.id,
        contentType: 'Loi Sapin - Texte d\'introduction',
        contentTitle: 'Loi Sapin - Texte d\'introduction',
        modificationType: modificationType,
        contentDescription: result.introduction,
        author: result.updatedBy.firstname + " " + result.updatedBy.lastname
      }
    });
  },
};