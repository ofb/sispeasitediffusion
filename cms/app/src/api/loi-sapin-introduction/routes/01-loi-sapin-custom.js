module.exports = {
  routes: [
    {
      method: 'GET',
      path: '/loi-sapin-introduction/preview',
      handler: 'loi-sapin-introduction.findDraft',
    },
  ]
}
