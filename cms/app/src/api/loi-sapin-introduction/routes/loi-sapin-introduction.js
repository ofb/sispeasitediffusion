'use strict';

/**
 * loi-sapin-introduction router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::loi-sapin-introduction.loi-sapin-introduction');
