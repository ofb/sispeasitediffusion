'use strict';

/**
 * loi-sapin-introduction service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::loi-sapin-introduction.loi-sapin-introduction');
