'use strict';

/**
 *  professionnel-liens controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::professionnel-liens.professionnel-liens', ({ strapi }) => ({
  async find(ctx) {
    const { query } = ctx;

    const entity = await strapi.entityService.findMany('api::professionnel-liens.professionnel-liens', {
      ...query,
        populate: {
          liens: {
            populate: {
              libelle: true,
              entree_de_menu: {
                fields: ['nom', 'url'],
              },
              article: {
                fields: ['titre', 'identifiant'],
              },
              page: {
                fields: ['titre', 'identifiant'],
              },
            }
          }
        },
     });
     const sanitizedEntity = await this.sanitizeOutput(entity, ctx);

     return this.transformResponse(sanitizedEntity);
  },

}));
