'use strict';

/**
 * professionnel-liens router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::professionnel-liens.professionnel-liens');
