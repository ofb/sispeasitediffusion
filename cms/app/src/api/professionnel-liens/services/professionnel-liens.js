'use strict';

/**
 * professionnel-liens service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::professionnel-liens.professionnel-liens');
