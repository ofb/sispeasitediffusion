'use strict';

/**
 *  menu-principal controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::menu-principal.menu-principal', ({ strapi }) => ({
  async find(ctx) {
    const { query } = ctx;

    const entity = await strapi.entityService.findMany('api::menu-principal.menu-principal', {
      ...query,
      populate: {
        Sections: {
          populate: {
            liens: {
              populate: {
                libelle: true,
                entree_de_menu: {
                  fields: ['nom', 'url'],
                },
                article: {
                  fields: ['titre', 'identifiant'],
                },
                page: {
                  fields: ['titre', 'identifiant'],
                },
              }
            },
            Sous_sections: {
              populate: {
                liens: {
                  populate: {
                    libelle: true,
                    entree_de_menu: {
                      fields: ['nom', 'url'],
                    },
                    article: {
                      fields: ['titre', 'identifiant'],
                    },
                    page: {
                      fields: ['titre', 'identifiant'],
                    },
                  }
                }
              }
            }
          }
        }
      },
     });
     const sanitizedEntity = await this.sanitizeOutput(entity, ctx);

     return this.transformResponse(sanitizedEntity);
  },

}));

