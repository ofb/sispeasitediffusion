module.exports = {

  afterCreate(event) {
    const { result, params } = event;

    strapi.entityService.create('api::historique-modifications.historique-modifications', {
      data : {
        contentId: result.id,
        contentType: 'Menu principal',
        modificationType: 'Création',
        author: result.createdBy.firstname + " " + result.createdBy.lastname
      }
    });
  },

  afterUpdate(event) {
    const { result, params } = event;

    strapi.entityService.create('api::historique-modifications.historique-modifications', {
      data : {
        contentId: result.id,
        contentType: 'Menu principal',
        modificationType: 'Mise à jour',
        author: result.updatedBy.firstname + " " + result.updatedBy.lastname
      }
    });
  },
};