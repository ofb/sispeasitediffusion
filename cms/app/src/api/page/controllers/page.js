'use strict';

/**
 *  page controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::page.page', ({ strapi }) => ({

  async findOneBySlug(ctx) {
    const { identifiant } = ctx.params;

    const entity = await strapi.db.query('api::page.page').findOne({
            where: {
              $and: [
                {
                  identifiant: identifiant,
                },
                {
                  publishedAt: {
                    $ne: null
                  },
                },
              ],
            },
            populate: {
              illustration: true,
              seo: true,
              en_savoir_plus:  {
                populate: {
                  entree_de_menu: true,
                  article: {
                    populate: {
                      illustration: true,
                    },
                  },
                  page: true,
                }
              }
           }
        });
    const sanitizedEntity = await this.sanitizeOutput(entity, ctx);
    return this.transformResponse(sanitizedEntity);
  },

  async findOneDraftBySlug(ctx) {
    const { identifiant } = ctx.params;

    const entity = await strapi.db.query('api::page.page').findOne({
            where: {
              $and: [
                {
                  identifiant: identifiant,
                },
                {
                  publishedAt: null ,
                },
              ],
            },
            populate: {
              illustration: true,
              seo: true,
              en_savoir_plus:  {
                populate: {
                  entree_de_menu: true,
                  article: {
                    populate: {
                      illustration: true,
                    },
                  },
                  page: true,
                }
              }
           }
        });
    const sanitizedEntity = await this.sanitizeOutput(entity, ctx);
    return this.transformResponse(sanitizedEntity);
  }
}))
