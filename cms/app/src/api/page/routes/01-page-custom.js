module.exports = {
  routes: [
    {
      method: 'GET',
      path: '/page/:identifiant',
      handler: 'page.findOneBySlug',
    },
    {
      method: 'GET',
      path: '/page/preview/:identifiant',
      handler: 'page.findOneDraftBySlug',
    },
  ]
}
