'use strict';

/**
 *  actualite controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::actualite.actualite', ({ strapi }) => ({
  count(ctx) {
      var { query } = ctx.request
      return strapi.query('api::actualite.actualite').count({ where: query });
  },

  async findOne(ctx) {
    const { id } = ctx.params;

    const entity = await strapi.db.query('api::actualite.actualite').findOne({
            where: {
              $and: [
                {
                  id: id,
                },
                {
                  publishedAt: {
                    $ne: null
                  },
                },
              ],
            },
            populate: {
              illustration: true,
              seo: true,
              en_savoir_plus:  {
                populate: {
                  entree_de_menu: true,
                  article: {
                    populate: {
                      illustration: true,
                    },
                  },
                  page: true,
                }
              }
           }
        });
    const sanitizedEntity = await this.sanitizeOutput(entity, ctx);
    return this.transformResponse(sanitizedEntity);
  },

  async findOneDraft(ctx) {
    const { id } = ctx.params;

    const entity = await strapi.db.query('api::actualite.actualite').findOne({
            where: {
              $and: [
                {
                  id: id,
                },
                {
                  publishedAt: null ,
                },
              ],
            },
            populate: {
              illustration: true,
              seo: true,
              en_savoir_plus:  {
                populate: {
                  entree_de_menu: true,
                  article: {
                    populate: {
                      illustration: true,
                    },
                  },
                  page: true,
                }
              }
           }
        });
    const sanitizedEntity = await this.sanitizeOutput(entity, ctx);
    return this.transformResponse(sanitizedEntity);
  }
}))
