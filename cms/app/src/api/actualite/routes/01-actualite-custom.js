module.exports = {
  routes: [
    {
      method: 'GET',
      path: '/actualite/:id',
      handler: 'actualite.findOne',
    },
    {
      method: 'GET',
      path: '/actualite/preview/:id',
      handler: 'actualite.findOneDraft',
    },
  ]
}
