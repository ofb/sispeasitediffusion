module.exports = {
    routes: [
        { // Path defined with a URL parameter
            method: 'GET',
            path: '/actualites/count',
            handler: 'actualite.count',
        },
    ]
}
