'use strict';

/**
 * entree-de-menu service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::entree-de-menu.entree-de-menu');
