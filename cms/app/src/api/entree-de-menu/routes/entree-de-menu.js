'use strict';

/**
 * entree-de-menu router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::entree-de-menu.entree-de-menu');
