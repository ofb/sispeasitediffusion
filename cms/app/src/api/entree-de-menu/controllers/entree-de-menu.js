'use strict';

/**
 *  entree-de-menu controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::entree-de-menu.entree-de-menu');
