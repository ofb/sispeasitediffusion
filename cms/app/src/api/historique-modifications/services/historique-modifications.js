'use strict';

/**
 * historique-modifications service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::historique-modifications.historique-modifications');
