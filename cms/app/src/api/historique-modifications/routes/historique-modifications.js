'use strict';

/**
 * historique-modifications router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::historique-modifications.historique-modifications');
