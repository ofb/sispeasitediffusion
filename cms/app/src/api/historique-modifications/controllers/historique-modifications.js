'use strict';

/**
 *  historique-modifications controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::historique-modifications.historique-modifications');
