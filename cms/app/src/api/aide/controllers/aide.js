'use strict';

/**
 *  aide controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::aide.aide', ({ strapi }) => ({

  async findOneBySlug(ctx) {
    const { identifiant } = ctx.params;

    const entity = await strapi.db.query('api::aide.aide').findOne({
            where: {
              $and: [
                {
                  identifiant: identifiant,
                },
                {
                  publishedAt: {
                    $ne: null
                  },
                },
              ],
            }
        });
    const sanitizedEntity = await this.sanitizeOutput(entity, ctx);
    return this.transformResponse(sanitizedEntity);
  },

  async findOneDraftBySlug(ctx) {
    const { identifiant } = ctx.params;

    const entity = await strapi.db.query('api::aide.aide').findOne({
            where: {
              $and: [
                {
                  identifiant: identifiant,
                },
                {
                  publishedAt: null ,
                },
              ],
            }
        });
    const sanitizedEntity = await this.sanitizeOutput(entity, ctx);
    return this.transformResponse(sanitizedEntity);
  }
}));
