module.exports = {
  routes: [
    {
      method: 'GET',
      path: '/aide/:identifiant',
      handler: 'aide.findOneBySlug',
    },
    {
      method: 'GET',
      path: '/aide/preview/:identifiant',
      handler: 'aide.findOneDraftBySlug',
    },
  ]
}
