'use strict';

/**
 * aide service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::aide.aide');
