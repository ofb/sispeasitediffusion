module.exports = {

  afterCreate(event) {
    const { result, params } = event;

    strapi.entityService.create('api::historique-modifications.historique-modifications', {
      data : {
        contentId: result.id,
        contentType: 'Article',
        contentTitle: params.data.titre,
        modificationType: 'Création',
        contentDescription: params.data.contenu,
        author: result.createdBy.firstname + " " + result.createdBy.lastname
      }
    });
  },

  afterUpdate(event) {
    const { result, params } = event,
          modificationType = params.data.publishedAt === undefined ? 'Mise à jour' :
                             params.data.publishedAt === null ? 'Dépublication' : 'Publication';

      // console.log(result);
      // console.log(params);
    strapi.entityService.create('api::historique-modifications.historique-modifications', {
      data : {
        contentId: result.id,
        contentType: 'Article',
        contentTitle: result.titre,
        modificationType: modificationType,
        contentDescription: result.contenu,
        author: result.updatedBy.firstname + " " + result.updatedBy.lastname
      }
    });
  },
};