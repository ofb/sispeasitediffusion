module.exports = {
  routes: [
    {
      method: 'GET',
      path: '/article/:identifiant',
      handler: 'article.findOneBySlug',
    },
    {
      method: 'GET',
      path: '/article/preview/:identifiant',
      handler: 'article.findOneDraftBySlug',
    },
  ]
}
