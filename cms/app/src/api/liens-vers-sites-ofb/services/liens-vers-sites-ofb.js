'use strict';

/**
 * liens-vers-sites-ofb service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::liens-vers-sites-ofb.liens-vers-sites-ofb');
