'use strict';

/**
 *  liens-vers-sites-ofb controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::liens-vers-sites-ofb.liens-vers-sites-ofb', ({ strapi }) => ({
  async findAndSort(ctx) {

    let sortedLinks = {
      'Eau potable' : { categorie:  'Eau potable' , liens: [] },
      'Assainissement' : { categorie:  'Assainissement' , liens: [] },
      'Multi-domaines' : { categorie:  'Multi-domaines' , liens: [] },
      'Outils' : { categorie:  'Outils' , liens: [] },
      'Bassin' : { categorie:  'Bassin' , liens: [] },
      "Agences de l'eau" : { categorie:  "Agences de l'eau" , liens: [] },
      'Autres observatoires' : { categorie:  'Autres observatoires' , liens: [] }
    };

    let data = await strapi.db.query('api::liens-vers-sites-ofb.liens-vers-sites-ofb').findMany({
      populate: ['Image']
    });

    if (!data || !data.length) {
      return null;
    }

    data.forEach(entry => {
      let categorie = entry.Categorie;
      if (!sortedLinks[categorie]) {
        sortedLinks[categorie] = { categorie, liens: [] }
      }
      sortedLinks[categorie].liens.push({
        'nom' :  entry.Nom,
        'lien': entry.Lien,
        'image': entry.Image
      });
    });

    return Object.values(sortedLinks);
  },
}))
