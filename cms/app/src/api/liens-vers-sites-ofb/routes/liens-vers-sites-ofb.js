'use strict';

/**
 * liens-vers-sites-ofb router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::liens-vers-sites-ofb.liens-vers-sites-ofb');
