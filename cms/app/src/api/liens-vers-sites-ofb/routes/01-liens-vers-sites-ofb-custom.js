module.exports = {
  routes: [
    {
      method: 'GET',
      path: '/liens-vers-sites-ofb',
      handler: 'liens-vers-sites-ofb.findAndSort',
    }
  ]
}
