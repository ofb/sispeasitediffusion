module.exports = {

  afterCreate(event) {
    const { result, params } = event;

    strapi.entityService.create('api::historique-modifications.historique-modifications', {
      data : {
        contentId: result.id,
        contentType: 'Lien vers site OFB',
        contentTitle: params.data.Nom,
        modificationType: 'Création',
        author: result.createdBy.firstname + " " + result.createdBy.lastname
      }
    });
  },

  afterUpdate(event) {
    const { result, params } = event;

    strapi.entityService.create('api::historique-modifications.historique-modifications', {
      data : {
        contentId: result.id,
        contentType: 'Lien vers site OFB',
        contentTitle: result.Nom,
        modificationType: 'Mise à jour',
        author: result.updatedBy.firstname + " " + result.updatedBy.lastname
      }
    });
  },
};