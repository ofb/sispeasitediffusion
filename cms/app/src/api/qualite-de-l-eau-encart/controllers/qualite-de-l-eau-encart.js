'use strict';

/**
 *  qualite-de-l-eau-encart controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::qualite-de-l-eau-encart.qualite-de-l-eau-encart', ({ strapi }) => ({

    async findDraft(ctx) {
      const entity = await strapi.db.query('api::qualite-de-l-eau-encart.qualite-de-l-eau-encart').findOne({
          where: {
            publishedAt: null,
          }
        });
      const sanitizedEntity = await this.sanitizeOutput(entity, ctx);
      return this.transformResponse(sanitizedEntity);
    }
  
  }))
  