module.exports = {
  routes: [
    {
      method: 'GET',
      path: '/qualite-de-l-eau-encart/preview',
      handler: 'qualite-de-l-eau-encart.findDraft',
    },
  ]
}
