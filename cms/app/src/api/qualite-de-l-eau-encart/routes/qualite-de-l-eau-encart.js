'use strict';

/**
 * qualite-de-l-eau-encart router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::qualite-de-l-eau-encart.qualite-de-l-eau-encart');
