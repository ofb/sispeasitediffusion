'use strict';

/**
 * qualite-de-l-eau-encart service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::qualite-de-l-eau-encart.qualite-de-l-eau-encart');
