module.exports = {

  afterCreate(event) {
    const { result, params } = event;

    strapi.entityService.create('api::historique-modifications.historique-modifications', {
      data : {
        contentId: result.id,
        contentType: 'Page d\'accueil - Encart',
        contentTitle: params.data.Titre,
        modificationType: 'Création',
        contentDescription: params.data.Contenu,
        author: result.createdBy.firstname + " " + result.createdBy.lastname
      }
    });
  },

  afterUpdate(event) {
    const { result, params } = event,
          modificationType = params.data.publishedAt === undefined ? 'Mise à jour' :
                             params.data.publishedAt === null ? 'Dépublication' : 'Publication';

    strapi.entityService.create('api::historique-modifications.historique-modifications', {
      data : {
        contentId: result.id,
        contentType: 'Page d\'accueil - Encart',
        contentTitle: result.Titre,
        modificationType: modificationType,
        contentDescription: result.Contenu,
        author: result.updatedBy.firstname + " " + result.updatedBy.lastname
      }
    });
  },
};