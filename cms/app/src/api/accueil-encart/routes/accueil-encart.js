'use strict';

/**
 * accueil-encart router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::accueil-encart.accueil-encart');
