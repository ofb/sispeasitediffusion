'use strict';

/**
 * accueil-encart service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::accueil-encart.accueil-encart');
