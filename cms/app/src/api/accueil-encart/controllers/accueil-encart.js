'use strict';

/**
 *  accueil-encart controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::accueil-encart.accueil-encart', ({ strapi }) => ({

  async findDraft(ctx) {
    const entity = await strapi.db.query('api::accueil-encart.accueil-encart').findOne({
        where: {
          publishedAt: null,
        }
      });
    const sanitizedEntity = await this.sanitizeOutput(entity, ctx);
    return this.transformResponse(sanitizedEntity);
  }

}))

