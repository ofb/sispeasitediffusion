'use strict';

var pdfjs = require("pdfjs-dist/legacy/build/pdf.min.js");
// var PdfjsWorker = require("worker-loader?esModule=false&filename=[name].js!pdfjs-dist/legacy/build/pdf.worker.min.js");

// if (typeof window !== "undefined" && "Worker" in window) {
//   pdfjs.GlobalWorkerOptions.workerPort = new PdfjsWorker();
// }
const _ = require('lodash');
const { join } = require('path');

var fs = require('fs');

var Canvas = require ('canvas');

const writeStreamToFile = (stream, path) =>
  new Promise((resolve, reject) => {
    const writeStream = fs.createWriteStream(path);
    // Reject promise if there is an error with the provided stream
    stream.on('error', reject);
    stream.pipe(writeStream);
    writeStream.on('close', resolve);
    writeStream.on('error', reject);
  });

module.exports = {
  /**
   * An asynchronous register function that runs before
   * your application is initialized.
   *
   * This gives you an opportunity to extend code.
   */
  register({ strapi }) {

    strapi.contentType('plugin::upload.file').attributes.thumbnail = {
      type: 'text',
    }


    strapi.plugin('upload').services.upload.uploadFileAndPersist = async function (fileData, { user } = {}) {

      const config = strapi.config.get('plugin.upload');

      const { isImage } = strapi.plugins.upload.services['image-manipulation']; //= getService('image-manipulation');
  
      if (await isImage(fileData)) {
        const { getDimensions, generateThumbnail, generateResponsiveFormats, isOptimizableImage } = strapi.plugins.upload.services['image-manipulation'];
    
        // Store width and height of the original image
        const { width, height } = await getDimensions(fileData);
    
        // Make sure this is assigned before calling any upload
        // That way it can mutate the width and height
        _.assign(fileData, {
          width,
          height,
        });
    
        // For performance reasons, all uploads are wrapped in a single Promise.all
        const uploadThumbnail = async (thumbnailFile) => {
          await strapi.plugin('upload').service('provider').upload(thumbnailFile);
          _.set(fileData, 'formats.thumbnail', thumbnailFile);
        };
    
        const uploadResponsiveFormat = async (format) => {
          const { key, file } = format;
          await strapi.plugin('upload').service('provider').upload(file);
          _.set(fileData, ['formats', key], file);
        };
    
        const uploadPromises = [];
    
        // Upload image
        uploadPromises.push(strapi.plugin('upload').service('provider').upload(fileData));
    
        // Generate & Upload thumbnail and responsive formats
        if (await isOptimizableImage(fileData)) {
          const thumbnailFile = await generateThumbnail(fileData);
          if (thumbnailFile) {
            uploadPromises.push(uploadThumbnail(thumbnailFile));
          }
    
          const formats = await generateResponsiveFormats(fileData);
          if (Array.isArray(formats) && formats.length > 0) {
            for (const format of formats) {
              if (!format) continue;
              uploadPromises.push(uploadResponsiveFormat(format));
            }
          }
        }
        // Wait for all uploads to finish
        await Promise.all(uploadPromises);
      } else if(fileData.mime == 'application/pdf') {
          // Generate PDF thumbnail
          const uploadPromises = [];
          uploadPromises.push(strapi.plugin('upload').service('provider').upload(fileData));
          
          let file = fs.readFileSync(fileData.getStream().path);

          let loadingTask = pdfjs.getDocument(file);
          loadingTask.promise.then(function(pdf) {
  
                pdf.getPage(1).then(function(page) {

                var scale = 0.2;
                var viewport = page.getViewport({scale: scale});

                const canvas = Canvas.createCanvas(viewport.width, viewport.height);
                const context = canvas.getContext('2d');

                const renderContext = {
                    canvasContext: context,
                    viewport
                };

                page.render(renderContext).promise.then(() => {
                  fileData.thumbnail = canvas.toDataURL();
                });
              });
            });

            // Wait for all uploads to finish
            await Promise.all(uploadPromises);
      } else {
        await strapi.plugin('upload').service('provider').upload(fileData);
      }
  
      _.set(fileData, 'provider', config.provider);
  
      // Persist file(s)
      return this.add(fileData, { user });

    }
  },

  /**
   * An asynchronous bootstrap function that runs before
   * your application gets started.
   *
   * This gives you an opportunity to set up your data model,
   * run jobs, or perform some special logic.
   */
  bootstrap(/*{ strapi }*/) {},
};
