module.exports = [
  'strapi::errors',
  'strapi::cors',
  'strapi::poweredBy',
  'strapi::logger',
  'strapi::query',
  'strapi::body',
  'strapi::session',
  'strapi::favicon',
  'strapi::public',
  {
    name: "strapi::security",
    config: {
      contentSecurityPolicy: {
        useDefaults: true,
        directives: {
          "default-src": [
            "'self'",
            "data:",
            "blob:",
            "https://www.youtube.com/",
            "https://www.youtube.com/embed/",
            "https://player.vimeo.com/",
          ],
          upgradeInsecureRequests: null,
        },
      },
    },
  },
];
