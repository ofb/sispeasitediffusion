// path: ./config/plugins.js

module.exports = ({ env }) => ({
  'wysiwyg-ckeditor-5': {
    enabled: true,
    resolve: "./src/plugins/wysiwyg-ckeditor-5",
  },
  email: {
    config: {
      provider: 'nodemailer',
      providerOptions: {
        host: env('SMTP_HOST', '127.1.0.0'),
        port: env('SMTP_PORT', 587),
        secure: true,
        auth: {
          user: env('SMTP_USERNAME'),
          pass: env('SMTP_PASSWORD'),
        },
      },
    },
  },
});
