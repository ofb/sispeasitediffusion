# SispeaSiteDiffusion

Vitrine pour le grand public de l’Observatoire national des services d’eau et d’assainissement, recense et met à disposition les données des services publics d’eau potable, d’assainissement collectif et non collectif.

## Les différentes instances

* demo latest du site sispea diffusion : https://sispea-diffusion-latest.demo.codelutin.com
* demo latest du CMS : https://sispea-cms-latest.demo.codelutin.com


## Architecture

Le site est généré par un frontend [**Nuxt.js**](./webapp/README.md).

Les données alimentant le site de diffusion proviennent de plusieurs backend :
* [**Sispea Contributeur**](https://gitlab.nuiton.org/ofb/sispea/)
* [**CMS Strapi**](./cms/app/README.md)

La [documentation du projet](./documentation/README.md) est incluse dans le présent dépôt et générée via Docusaurus.
